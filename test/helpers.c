#include "helpers.h"

/**
 * Takes two filestreams and compares them on a line by line basis,
 * reporting to stderr and exiting on the first mismatch encountered.
 */
int diff(FILE *file_expected, FILE *file_received)
{
    char line_expected[DIFF_LINE_BUFFER_SIZE];
    char line_received[DIFF_LINE_BUFFER_SIZE];
    unsigned int line_number = 1;
    char *ret_expected;
    char *ret_received;

    while (1) {
        ret_expected = fgets(line_expected, sizeof(line_expected), file_expected);
        ret_received = fgets(line_received, sizeof(line_received), file_received);

        if ((ret_expected == NULL) != (ret_received == NULL) ||
            strcmp(line_expected, line_received) != 0) {
            fprintf(stderr,
                    "\nLine %d:\n%s%s%s%s%s%s\n",
                    line_number,
                    COLOR_RED,
                    line_expected,
                    COLOR_RESET,
                    COLOR_GREEN,
                    line_received,
                    COLOR_RESET);
            return 0;
        }

        if (feof(file_expected) || feof(file_received))
            return 1;

        line_number++;
    }
}