#ifndef ENO_H
#define ENO_H

#include <stdio.h>

#define ENO_MAX_ERROR_SIZE 256

typedef struct ENOIteratorOpaque {
    void *internal[3];
} ENOIterator;

unsigned int eno_compare_key(ENOIterator *iterator, char *key);

void eno_debug_document(ENOIterator *iterator);

void eno_free_document(ENOIterator *iterator);

unsigned int eno_get_key(ENOIterator *iterator, char **buffer, size_t *size);
unsigned int eno_get_value(ENOIterator *iterator, char **buffer, size_t *size);

unsigned int eno_has_attributes(ENOIterator *iterator);
unsigned int eno_has_children(ENOIterator *iterator);
unsigned int eno_has_items(ENOIterator *iterator);
unsigned int eno_has_key(ENOIterator *iterator);
unsigned int eno_has_value(ENOIterator *iterator);

unsigned int eno_is_attribute(ENOIterator *iterator);
unsigned int eno_is_document(ENOIterator *iterator);
unsigned int eno_is_embed(ENOIterator *iterator);
unsigned int eno_is_field(ENOIterator *iterator);
unsigned int eno_is_flag(ENOIterator *iterator);
unsigned int eno_is_item(ENOIterator *iterator);
unsigned int eno_is_section(ENOIterator *iterator);

unsigned int eno_iterate_children(ENOIterator *parent_iterator, ENOIterator *child_iterator);
unsigned int eno_iterate_next(ENOIterator *iterator);

unsigned int eno_parse_memory(ENOIterator *iterator, char *buffer, size_t size);
unsigned int eno_parse_stream(ENOIterator *iterator, FILE *stream);

void eno_print_error_to_memory(ENOIterator *iterator, char *buffer, size_t size);
void eno_print_error_to_stream(ENOIterator *iterator, FILE *stream);

#endif
