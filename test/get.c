#include <stdio.h>

#include "eno.h"

int main(int argc, char *argv[])
{
    if(argc < 3) {
        printf("Usage: %s DOCUMENT_PATH ELEMENT_KEY\n", argv[0]);
    } else {
        FILE *file  = fopen(argv[1], "rb");
        ENOIterator iterator;

        if (eno_parse_stream(&iterator, file)) {
            while (eno_iterate_next(&iterator)) {
                if (eno_is_field(&iterator) && eno_compare_key(&iterator, argv[2])) {
                    char *string;
                    size_t size;

                    eno_get_value(&iterator, &string, &size);

                    if (size > 0) {
                        printf("A field with key '%s' was found, it's value is: '%.*s'.\n", argv[2], (unsigned int)size, string);
                    } else {
                        printf("A field with key '%s' was found, it's empty or contains attributes or items.\n", argv[2]);
                    }
                }
            }
        } else {
            eno_print_error_to_stream(&iterator, stderr);
            eno_debug_document(&iterator);
        }

        eno_free_document(&iterator);
    }

    return 0;
}
