#ifndef ENO_INSTANCE_H
#define ENO_INSTANCE_H

#include <unicode/ucol.h>

#include "instructions.h"

struct Error {
    int code;
    size_t instruction_offset;
};

struct ICUContext {
    UCollator *collator;
    UErrorCode status;
};

/**
 * Instructions contains an arbitrary number of sequential struct allocations of
 * mixed element types.
 */
struct Instance {
    struct Error error;
    struct ICUContext icu_context;
    char *input_buffer;
    unsigned int input_linecount;
    size_t input_size;
    char *instructions;
};

#endif