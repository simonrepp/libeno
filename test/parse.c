#include <stdio.h>

#include "eno.h"

int main(int argc, char *argv[])
{
    if(argc < 2) {
        printf("Usage: %s DOCUMENT_PATH\n", argv[0]);
    } else {
        FILE *file  = fopen(argv[1], "rb");
        ENOIterator iterator;

        if (eno_parse_stream(&iterator, file)) {
            eno_debug_document(&iterator);
        } else {
            eno_print_error_to_stream(&iterator, stderr);
            eno_debug_document(&iterator);
        }

        eno_free_document(&iterator);
    }

    return 0;
}
