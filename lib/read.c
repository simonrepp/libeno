#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include "error.h"
#include "read.h"

#define READ_CHUNK_SIZE 262144

/**
 * Read provided stream into memory in chunks.
 */
void read_stream(struct Instance *instance, FILE *stream)
{
    size_t bytes_read;
    size_t buffer_capacity = 0;
    char *buffer_memo;

    instance->input_buffer = NULL;
    instance->input_size = 0;

    if (stream == NULL || ferror(stream)) {
        instance->error.code = READ_ERROR_STREAM_ERROR;
        return;
    }

    while (1) {
        if (instance->input_size + READ_CHUNK_SIZE > buffer_capacity) {
            buffer_capacity = instance->input_size + READ_CHUNK_SIZE;

            buffer_memo = instance->input_buffer;
            instance->input_buffer = realloc(instance->input_buffer, buffer_capacity);

            if (instance->input_buffer == NULL) {
                free(buffer_memo);
                instance->error.code = READ_ERROR_OUT_OF_MEMORY;
                return;
            }
        }

        bytes_read = fread(instance->input_buffer + instance->input_size, 1, READ_CHUNK_SIZE, stream);

        if (bytes_read == 0)
            break;

        instance->input_size += bytes_read;
    }

    if (ferror(stream)) {
        free(instance->input_buffer);
        instance->input_buffer = NULL;
        instance->error.code = READ_ERROR_STREAM_ERROR;
        return;
    }
    
    if (instance->input_size == 0) {
        free(instance->input_buffer);
        instance->input_buffer = NULL;
    } else {
        buffer_memo = instance->input_buffer;
        instance->input_buffer = realloc(instance->input_buffer, instance->input_size);

        if (instance->input_buffer == NULL) {
            free(buffer_memo);
            instance->error.code = READ_ERROR_OUT_OF_MEMORY;
        }        
    }
}
