#include "eno.h"
#include "iterate.h"
#include "parse.h"

/**
 * Initializes a child iterator for the children of the parent iterator's element.
 * The iterator only references an element after the first eno_iterate_next call.
 *
 * Note that it's explicitly allowed to pass the same iterator both as parent and
 * child iterator to this function to move the iterator down a hierarchy level.
 *
 * Also note that for the document itself this is a noop, in that case you can
 * just call eno_iterate_next() to advance to the first element of the document.
 *
 * @return 0 if the parent iterator has been exhausted or the iterated element has no children, otherwise 1
 */
unsigned int eno_iterate_children(ENOIterator *opaque_parent_iterator, ENOIterator *opaque_child_iterator)
{
    struct Iterator *child_iterator = (struct Iterator *)opaque_child_iterator;
    struct Iterator *parent_iterator = (struct Iterator *)opaque_parent_iterator;
    struct Instance *instance = parent_iterator->instance;
    struct Instruction *parent_element = parent_iterator->current_instruction;

    child_iterator->instance = parent_iterator->instance;

    if (parent_element == NULL || !(parent_element->id & HAS_CHILDREN)) {
        child_iterator->current_instruction = NULL;
        child_iterator->initial_instruction = NULL;
        return 0;
    }

    if (parent_element->id & IS_DOCUMENT) {
        child_iterator->current_instruction = parent_element;
        child_iterator->initial_instruction = NULL;
    } else if (parent_element->id & IS_FIELD) {
        child_iterator->current_instruction = NULL;

        if (parent_element->id & IS_ESCAPED) {
            child_iterator->initial_instruction = (struct Instruction *)(instance->instructions + ((struct FieldEscapedWithChildren *)parent_element)->first_child_offset);
        } else {
            child_iterator->initial_instruction = (struct Instruction *)(instance->instructions + ((struct FieldWithChildren *)parent_element)->first_child_offset);
        }
    } else /* if (parent_element->id & IS_SECTION) */ {
        child_iterator->current_instruction = NULL;
        child_iterator->initial_instruction = (struct Instruction *)(instance->instructions + ((struct Section *)parent_element)->first_child_offset);
    }

    return 1;
}

/**
 * Iterates to the first element of the document, field or section on a
 * freshly initialized iterator. On an already used iterator, advances
 * to the next element.
 *
 * @return 0 if the iterator has been exhausted, otherwise 1
 */
unsigned int eno_iterate_next(ENOIterator *opaque_iterator)
{
    struct Iterator *iterator = (struct Iterator *)opaque_iterator;

    if (iterator->current_instruction != NULL) {
        if (iterator->current_instruction->next_sibling_offset == NONE) {
            iterator->current_instruction = NULL;
        } else {
            iterator->current_instruction = (struct Instruction *)(iterator->instance->instructions + iterator->current_instruction->next_sibling_offset);
        }
    } else if (iterator->initial_instruction != NULL) {
        iterator->current_instruction = iterator->initial_instruction;
        iterator->initial_instruction = NULL;
    }

    return iterator->current_instruction != NULL;
}
