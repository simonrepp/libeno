#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "eno.h"
#include "helpers.h"

#define ERROR_EXAMPLES_DIR "../test/error_examples"

int verify_parse_error(char *example_path, char *spec_path, unsigned int update_specs);

/**
 * Scans all entries in error_examples/, performing the following checks:
 * - TODO: Document
 */
int main()
{
    char example_path[512];
    DIR *examples_dir;
    struct dirent *examples_dirent;
    DIR *spec_dir;
    char spec_path[1024];
    struct stat statbuf;
    unsigned int update_specs = getenv("UPDATE_SPECS") != NULL;

    examples_dir = opendir(ERROR_EXAMPLES_DIR);

    while ((examples_dirent = readdir(examples_dir)) != NULL) {
        if (examples_dirent->d_name[0] == '.')
            continue;

        sprintf(example_path, "%s/%s", ERROR_EXAMPLES_DIR, examples_dirent->d_name);

        if (stat(example_path, &statbuf) || S_ISDIR(statbuf.st_mode))
            continue;

        fprintf(stderr, "%s\n", example_path);

        sprintf(spec_path,
                "%s/%.*s.spec",
                ERROR_EXAMPLES_DIR,
                (unsigned int)(strlen(examples_dirent->d_name) - strlen(".eno")),
                examples_dirent->d_name);

        if (!(spec_dir = opendir(spec_path))) {
            mkdir(spec_path, 0755);
        } else {
            closedir(spec_dir);
        }

        if (!verify_parse_error(example_path, spec_path, update_specs))
            return EXIT_FAILURE;
    }
    
    closedir(examples_dir);

    fprintf(stderr, "%sAll examples passed.%s\n", COLOR_GREEN, COLOR_RESET);

    return EXIT_SUCCESS;
}

/**
 * TODO: Document
 */
int verify_parse_error(char *example_path, char *spec_path, unsigned int update_specs)
{
    FILE *diff_file;
    char diff_path[512];
    FILE *example_file;
    ENOIterator example_iterator;
    FILE *out_file;
    char out_path[512];
    unsigned int success;

    example_file = fopen(example_path, "rb");

    if ((success = !eno_parse_stream(&example_iterator, example_file))) {
        sprintf(out_path, "%s/error.txt", spec_path);

        if (update_specs || !(out_file = fopen(out_path, "rb"))) {
            out_file = fopen(out_path, "wb");
            eno_print_error_to_stream(&example_iterator, out_file);
        } else {
            sprintf(diff_path, "%s/error.diff.txt", spec_path);

            diff_file = fopen(diff_path, "wb");
            eno_print_error_to_stream(&example_iterator, diff_file);
            fclose(diff_file);

            diff_file = fopen(diff_path, "rb");
            if ((success = diff(out_file, diff_file))) {
                fclose(diff_file);
                remove(diff_path);
            } else {
                fclose(diff_file);
                fprintf(stderr,
                        "Expected: %s%s%s\nReceived: %s%s%s\n\n%sAn example failed during parse error verification.%s\n",
                        COLOR_RED,
                        out_path,
                        COLOR_RESET,
                        COLOR_GREEN,
                        diff_path,
                        COLOR_RESET,
                        COLOR_RED,
                        COLOR_RESET);
            }
        }

        fclose(out_file);
    } else {
        fprintf(stderr, "FAILURE: %s did not error, although it should.\n", example_path);
    }

    eno_free_document(&example_iterator);
    fclose(example_file);

    return success;
}
