#include <unicode/ucol.h>

#include "eno.h"
#include "instructions.h"
#include "iterate.h"
#include "parse.h"

/**
 * Compares the key of the currently iterated element to the supplied null-terminated string.
 *
 * @return 1 if the keys match, 0 if they don't match or the iterated element has no key
 */
unsigned int eno_compare_key(ENOIterator *opaque_iterator, char *key)
{
    struct Instance *instance = ((struct Iterator *)opaque_iterator)->instance;
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    UCollationResult result;

    if (instruction == NULL || !(instruction->id & HAS_KEY))
        return 0;

    result = ucol_strcollUTF8(instance->icu_context.collator,
                              ((struct InstructionWithKey *)instruction)->key.begin,
                              ((struct InstructionWithKey *)instruction)->key.size,
                              key,
                              -1,
                              &instance->icu_context.status);

    return result == UCOL_EQUAL;
}
