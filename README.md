# libeno

[Eno](https://eno-lang.org/) parsing library for C

## Getting started

```c
#include <eno.h>
#include <string.h>

int main()
{
    char *content = "Who to greet: World";
    ENOIterator iterator;

    if (eno_parse_memory(&iterator, content, strlen(content))) {
        char *ptr;
        size_t size;

        eno_iterate_next(&iterator); /* advance to first element */

        if (eno_get_value(&iterator, &ptr, &size)) {
          printf("Hello %.*s!", (int)size, ptr);
        }
    } else {
        eno_report_error(&iterator);
    }

    eno_free_document(&iterator);

    return 0;
}
```

## Development status

libeno already implements the full Eno specification. Some parts are still
interim implementations, expect occassional full-on crashs for certain input or
usage patterns (please do report those, happy to resolve them).

The API is partially documented through doc comments, please be aware that
there are not yet any stability guarantees at this point.

In addition to the basic parser stuff you'd expect there is also some
functionality for printing back the AST that libeno constructs during parsing,
annotated with line numbers, both in plain text and with terminal coloring.

## String encoding

Eno is always encoded in UTF-8, and so is the resulting content of all
string buffers extracted from the document and provided to library consumers
after parsing. The [UTF-8 Everywhere Manifesto](https://utf8everywhere.org/)
formulates a good rationale for this if you're curious about some of the
reasoning for this design decision.

## Building from source

### Prerequisite

You need to install `icu4c` (the ICU library for C), including its headers.

On linux this can be done through the package manager (e.g. `libicu-dev` on Ubuntu).

### Using meson (recommended)

```bash
meson build
cd build
meson compile
```

After building you can optionally install libeno's shared library and header files to your system by running (inside `build/`):

```bash
meson install
```

### Using CMake

```bash
mkdir build
cd build
cmake ..
make
```

There is no install target configured in the CMake build manifest yet.

## Test

### Parsing correctness test suite

Inside `build/` run:

```
./test_examples
```

This scans through the `.eno` files in `test/examples/` and for each of them,
after parsing, creates serializations of both the internally and externally
accessible representation of the abstract syntax tree of the document, which are
then stored in a directory named after the example but postfixed with `.spec`
instead of `.eno`. These files are tracked with git and on consecutive runs the
serializations are re-generated and compared to the previously generated
snapshots, with any mismatches triggering errors and printing a diff for the
affected line. These specs serve to ensure that the parser behaves according to
specification and that regressions are quickly noticed during development.
Furthermore this serves as an examplified version of the specification because
more complex behavior can easily be studied by looking at the `.eno` files and
their respective `.spec` directory counterparts, which reveal how documents are
tokenized, continuations are assembled, etc.

To force an update of all specs you can run:

```
UPDATE_SPECS=yeah ./test_examples
```

### Parsing a document, printing the AST

Inside `build/` create a test document and run:

```
./test_parse your_document_path.eno
```

This debug-prints the document's abstract syntax tree and
reports errors in the document if it encounters one.

### Parsing a document, obtaining a value by key

Inside `build/` create a test document and run:

```
./test_get your_document_path.eno "some key"
```

This parses the document and prints the value of a field at the root level of
the document that matches the supplied key.

## Building the Documentation

The documentation uses Doxygen, which can be installed from package manager on most linux distributions.

When doxygen is installed go to the root of the repository and run:

```bash
doxygen
```

This generates the folder `docs/` which contains html documentation which you can view in your browser.

## License

libeno is licensed under the [LGPLv3+](https://www.gnu.org/licenses/lgpl-3.0.html).
