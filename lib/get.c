#include <stdlib.h>
#include <string.h>

#include "codepoints.h"
#include "eno.h"
#include "instance.h"
#include "instructions.h"
#include "iterate.h"

/**
 * Get the key of the currently iterated element.
 *
 * Sets the passed pointer to point to the beginning of the key and
 * stores the byte length of the key in the passed size variable.
 *
 * In case the iterator is already used up or when an element is iterated
 * that does not have a key, this function sets buffer to NULL and size to 0.
 *
 * Note that strings are never null-terminated, you only get a pointer
 * into the full document buffer along with the length of the string.
 *
 * @returns 0 if no key was obtained, 1 if there was
 */
unsigned int eno_get_key(ENOIterator *opaque_iterator, char **buffer, size_t *size)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;

    if (instruction != NULL && instruction->id & HAS_KEY) {
        *buffer = ((struct Flag *)instruction)->key.begin; /* All instruction structs holding a key store it in same place, */
        *size = ((struct Flag *)instruction)->key.size;    /* we use Flag as an arbitrary proxy to access the memory location. */
        return 1;
    }

    *buffer = NULL;
    *size = 0;
    return 0;
}

/**
 * Get the value of the currently iterated element.
 *
 * Sets the passed pointer to point to the beginning of the value and
 * stores the byte length of the value in the passed size variable.
 *
 * In case the iterator is already used up or when an element is iterated
 * that does not have a value, this function sets buffer to NULL and size to 0.
 *
 * Note that strings are never null-terminated, you only get a pointer
 * into the full document buffer along with the length of the string.
 *
 * @returns 0 if no value was obtained, 1 if there was
 */
unsigned int eno_get_value(ENOIterator *opaque_iterator, char **buffer, size_t *size)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;

    if (instruction == NULL || !(instruction->id & HAS_VALUE)) {
        *buffer = NULL;
        *size = 0;
        return 0;
    }

    if (instruction->id & IS_EMBED) {
        *buffer = ((struct Embed *)instruction)->value.begin;
        *size = ((struct Embed *)instruction)->value.size;
    } else if (instruction->id & IS_ITEM) {
        *buffer = ((struct ItemWithValue *)instruction)->value.begin;
        *size = ((struct ItemWithValue *)instruction)->value.size;
    } else if (instruction->id & IS_ESCAPED) {
        /* AttributeEscaped, FieldEscaped */
        *buffer = ((struct FieldEscapedWithValue *)instruction)->value.begin;
        *size = ((struct FieldEscapedWithValue *)instruction)->value.size;
    } else {
        /* Attribute, Field */
        *buffer = ((struct FieldWithValue *)instruction)->value.begin;
        *size = ((struct FieldWithValue *)instruction)->value.size;
    }

    return 1;
}
