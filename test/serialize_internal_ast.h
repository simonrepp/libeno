#ifndef ENO_SERIALIZE_INTERNAL_AST_H
#define ENO_SERIALIZE_INTERNAL_AST_H

#include "../lib/instance.h"
#include "../lib/instructions.h"
#include "../lib/parse.h"

#include "eno.h"

struct SerializeInternalAstContext {
    struct Instance *instance;
    unsigned long int line_number;
    size_t next_comment_offset;
    FILE *out_stream;
};

void serialize_internal_ast_attribute(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_attribute_escaped(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_comment(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_document(ENOIterator *opaque_iterator, FILE *out_stream);
void serialize_internal_ast_elements(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_embed(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_empty_line(struct SerializeInternalAstContext *context);
void serialize_internal_ast_field(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_field_children(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_field_escaped(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_flag(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_flag_escaped(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_item(struct SerializeInternalAstContext *context, size_t offset);
void serialize_internal_ast_preceding_idle_instructions(struct SerializeInternalAstContext *context, unsigned long int before_line_number);
void serialize_internal_ast_section(struct SerializeInternalAstContext *context, size_t offset);

#endif
