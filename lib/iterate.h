#ifndef ENO_ITERATE_H
#define ENO_ITERATE_H

#include "parse.h"

/**
 * TODO: Outdated documentation
 *
 * After initialization with eno_iterate_open, current_instruction points to NULL,
 * only after the first eno_iterate_next call the iterator can actually be used
 * to act on an element.
 */
struct Iterator {
    struct Instruction *current_instruction;
    struct Instance *instance;
    struct Instruction *initial_instruction;
};

#endif
