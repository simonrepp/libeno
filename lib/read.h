#ifndef ENO_READ_H
#define ENO_READ_H

#include <stdio.h>

#include "instance.h"

void read_stream(struct Instance *instance, FILE *stream);

#endif
