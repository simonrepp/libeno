#include <assert.h>
#include <errno.h>
#include <locale.h>
#include <stdlib.h>

/* TODO: Revisit if all of these are still needed (especially ustdio, ustring) */
#include <unicode/ubrk.h>
#include <unicode/ucol.h>
#include <unicode/ustdio.h>
#include <unicode/ustring.h>

#include "codepoints.h"
#include "iterate.h"
#include "parse.h"
#include "read.h"

void attach_element_at_current_section_level(struct ParseContext *context, size_t size)
{
    struct Instruction *element = (struct Instruction *)(context->instance->instructions + context->instructions_size);
    
    if (context->last_section_level_instruction_offset_stack[context->section_level] == NONE) {
        struct Section *section = (struct Section *)(context->instance->instructions + context->last_section_level_instruction_offset_stack[context->section_level - 1]);
        
        section->id |= HAS_CHILDREN;
        section->first_child_offset = context->instructions_size;
    } else {
        struct Instruction *last_section_level_instruction = (struct Instruction *)(context->instance->instructions + context->last_section_level_instruction_offset_stack[context->section_level]);
        last_section_level_instruction->next_sibling_offset = context->instructions_size;
    }

    context->last_section_level_instruction_offset_stack[context->section_level] = context->instructions_size;
    element->line.size = &context->instance->input_buffer[context->input_offset] - element->line.begin;
    context->instructions_size += size;
}

unsigned int eno_parse_internal(ENOIterator *opaque_iterator, FILE *stream, char *buffer, size_t size)
{
    struct Instance *instance = malloc(sizeof(*instance));
    struct Iterator *iterator = (struct Iterator *)opaque_iterator;
    char *locale;

    instance->error.code = 0;
    instance->icu_context.status = U_ZERO_ERROR;

    /* TODO: Investigate whether using the "default locale collation rules" (due to passing NULL as locale) is appropriate here */
    /* (See https://unicode-org.github.io/icu-docs/apidoc/released/icu4c/ucol_8h.html#a4721e4c0a519bb0139a874e191223590) */
    instance->icu_context.collator = ucol_open(NULL, &instance->icu_context.status);

    if (U_FAILURE(instance->icu_context.status)) {
      fputs("ucol_open failed\n", stderr);
      return -666; /* TODO: Model document-unrelated error reporting into the architecture */
    }

    /* TODO: We probably don't need this if we only determine grapheme cluster boundaries */
    /* Word breaks are locale-specific, so we'll obtain LC_CTYPE from the environment */
    if (!(locale = setlocale(LC_CTYPE, ""))) {
        fputs("Cannot determine system locale\n", stderr);
        return -666; /* TODO: Error codes for non-document-related errors */
    }

    if (stream == NULL) {
        instance->input_buffer = buffer;
        instance->input_size = size;
    } else {
        read_stream(instance, stream);
    }

    iterator->initial_instruction = NULL;
    iterator->instance = instance;

    if (instance->error.code == 0) {
        parse_document(instance);

        /* Ensure that we do NOT free the input_buffer later if we didn't allocate it ourselves */
        if (stream == NULL)
            instance->input_buffer = NULL;

        if (instance->error.code == 0) {
            iterator->current_instruction = (struct Instruction *)instance->instructions;
            return 1;
        }
    }

    iterator->current_instruction = NULL;
    return 0;
}

/**
 * Parses the supplied memory location and allocates a document.
 * Caller must free using eno_free_document(), also when there
 * are errors in the document. The memory location passed stays
 * in posession of the caller, and it's therefore the caller's
 * responsibility to free later. Note however that this can
 * only be done when no further action is performed on anything
 * returned from libeno anymore - the input memory is never copied
 * but only referenced.
 *
 * @returns 1 if parsing succeeded, otherwise 0
 */
unsigned int eno_parse_memory(ENOIterator *opaque_iterator, char *buffer, size_t size)
{
    return eno_parse_internal(opaque_iterator, NULL, buffer, size);
}

/**
 * Allocates required structures, parses the supplied input from the passed
 * stream and constructs the document tree. The passed iterator is initialized
 * at the document root, where you can call eno_iterate_next() to move to the
 * first element in the document.
 *
 * Caller must free all allocated data by using eno_free_document() later,
 * including when there are errors in the document.
 *
 * @returns 1 if parsing succeeded, otherwise 0
 */
unsigned int eno_parse_stream(ENOIterator *opaque_iterator, FILE *stream)
{
    return eno_parse_internal(opaque_iterator, stream, NULL, 0);
}

/**
 * Makes sure the instructions buffer is large enough to hold at least
 * one more instance of the largest instruction we can possibly assign.
 */
void ensure_instructions_capacity(struct ParseContext *context)
{
    if (context->instructions_capacity - context->instructions_size < sizeof(union AllInstructions)) {
        void *new_buffer;
        
        do {
            context->instructions_capacity *= 1.5;
        } while (context->instructions_capacity - context->instructions_size < sizeof(union AllInstructions));
        
        new_buffer = realloc(context->instance->instructions, context->instructions_capacity);

        if (new_buffer == NULL) {
            /* TODO: How do we adequately crash and burn here? Is this worth handling at all? */
            free(context->instance->instructions);
            context->instance->instructions = NULL;
            return;
        }
        
        context->instance->instructions = new_buffer;
    }
}

void parse_document(struct Instance *instance)
{
    struct ParseContext context;

    context.input_offset = 0;
    context.instance = instance;
    context.instructions_capacity = 65536; /* TODO: Un-hardcode and replace with prediction formula (macro?) - approximate_capacity(instance->input_size) => size_t */
    context.instructions_size = sizeof(struct Document);
    context.last_section_level_instruction_offset_stack[0] = 0;  /* zero offset to Document */
    context.last_section_level_instruction_offset_stack_size = STATIC_LAST_NEXT_ELEMENT_POINTER_STACK_SIZE;
    context.line_index = 0;
    context.next_comment_offset_variable_offset = offsetof(struct Document, first_comment_offset);
    context.next_unparsed_instruction_offset_variable_offset = offsetof(struct Document, first_unparsed_instruction_offset);
    context.section_level = 0;
    
    instance->instructions = malloc(context.instructions_capacity);

    {
        struct Document *document = (struct Document *)instance->instructions;
        document->first_comment_offset = NONE;
        document->first_unparsed_instruction_offset = NONE;
        document->id = IS_DOCUMENT;
        document->next_sibling_offset = NONE;
        /* We let document fall out of scope because its pointer address can
           change during parsing and therefore it must not be used afterwards */
    }

    while (context.input_offset < context.instance->input_size) {
        struct Instruction *element;
        
        ensure_instructions_capacity(&context);
        
        element = (struct Instruction *)(instance->instructions + context.instructions_size);
        
        element->line.begin = &context.instance->input_buffer[context.input_offset];

        skip_whitespace(&context);

        if (!NEWLINE(context.instance->input_buffer[context.input_offset])) {
            element->line_number = context.line_index + 1;
            element->next_sibling_offset = NONE;

            if (instance->error.code == 0) {
                switch (context.instance->input_buffer[context.input_offset]) {
                    case CODEPOINT_GREATER_THAN_SIGN:
                        read_comment(&context);
                        break;
                    case CODEPOINT_HASH:
                        read_section(&context);
                        break;
                    case CODEPOINT_DASH:
                        if (context.input_offset + 1 < context.instance->input_size &&
                            context.instance->input_buffer[context.input_offset + 1] == CODEPOINT_DASH) {
                            read_embed(&context);
                        } else {
                            read_item(&context);
                        }
                        break;
                    case CODEPOINT_COLON:
                        read_erratic(&context, PARSE_ERROR_INVALID_INSTRUCTION);
                        break;
                    case CODEPOINT_BACKTICK:
                        read_attribute_field_flag_escaped(&context);
                        break;
                    default:
                        read_attribute_field_flag(&context);
                }
            } else {
                read_unparsed(&context);
            }
        }

        /*
         *     / * if terminated by CR, eat LF * /
         *     if (buffer[codeunit_index] == 0xd && (codeunit = u_fgetc(file)) != 0xa)
         *         u_fungetc(codeunit, file);
         */

        context.input_offset++; /* advance to start of next line */
        context.line_index++;
    }

    {
        struct Document *document = (struct Document *)instance->instructions;
        if (document->next_sibling_offset != NONE)
            document->id |= HAS_CHILDREN;
    }

    instance->input_linecount = context.line_index; /* TODO: Revisit to ensure there is no one-off issue here */

    /* TODO: Consider resizing (= shrinking) down the allocated instructions memory
             if it is not fully used (which it will hardly ever be). */
}

void read_attribute(struct ParseContext *context)
{
    struct Attribute *attribute = (struct Attribute *)(context->instance->instructions + context->instructions_size);
    struct Instruction *last_field_level_instruction;
    struct Instruction *last_section_level_instruction;

    if (context->last_section_level_instruction_offset_stack[context->section_level] == NONE) {
        read_erratic(context, PARSE_ERROR_ATTRIBUTE_OUTSIDE_FIELD);
        return;
    }
    
    last_section_level_instruction = (struct Instruction *)(context->instance->instructions + context->last_section_level_instruction_offset_stack[context->section_level]);
    
    if (!(last_section_level_instruction->id & IS_FIELD)) {
        read_erratic(context, PARSE_ERROR_ATTRIBUTE_OUTSIDE_FIELD);
        return;
    }

    if (last_section_level_instruction->id & (HAS_VALUE | HAS_ITEMS)) {
        read_erratic(context, PARSE_ERROR_MIXED_FIELD_CONTENT);
        return;
    }

    attribute->id |= IS_ATTRIBUTE;
    attribute->operator.begin = &context->instance->input_buffer[context->input_offset];
    attribute->operator.size = 1;

    context->input_offset++;

    skip_whitespace(context);
    
    last_field_level_instruction = (struct Instruction *)(context->instance->instructions + context->last_field_level_instruction_offset);

    if (last_field_level_instruction->id & IS_ATTRIBUTE) {
        last_field_level_instruction->next_sibling_offset = context->instructions_size;
    } else if (last_field_level_instruction->id & IS_ESCAPED) {
        ((struct FieldEscapedWithChildren *)last_field_level_instruction)->first_child_offset = context->instructions_size;
    } else {
        ((struct FieldWithChildren *)last_field_level_instruction)->first_child_offset = context->instructions_size;
    }
    context->last_field_level_instruction_offset = context->instructions_size;

    last_section_level_instruction->id |= HAS_ATTRIBUTES | HAS_CHILDREN;

    if (NEWLINE(context->instance->input_buffer[context->input_offset])) {
        context->instructions_size += sizeof(*attribute);
    } else {
        struct AttributeWithValue *attribute_with_value = (struct AttributeWithValue *)attribute;

        attribute_with_value->id |= HAS_VALUE;

        read_token(context, &attribute_with_value->value);

        context->instructions_size += sizeof(*attribute_with_value);
    }

    attribute->line.size = &context->instance->input_buffer[context->input_offset] - attribute->line.begin;
}

void read_attribute_field_flag(struct ParseContext *context)
{
    struct InstructionWithKey *instruction = (struct InstructionWithKey *)(context->instance->instructions + context->instructions_size);

    instruction->id = HAS_KEY;
    instruction->key.begin = &context->instance->input_buffer[context->input_offset];

    while (1) {
        context->input_offset++;

        if (context->input_offset >= context->instance->input_size ||
            NEWLINE(context->instance->input_buffer[context->input_offset])) {
            read_flag(context);
            break;
        } else if (context->instance->input_buffer[context->input_offset] == CODEPOINT_COLON) {
            read_field(context);
            break;
        } else if (context->instance->input_buffer[context->input_offset] == CODEPOINT_EQUALS_SIGN) {
            read_attribute(context);
            break;
        } else if (!WHITESPACE(context->instance->input_buffer[context->input_offset])) {
            instruction->key.size = &context->instance->input_buffer[context->input_offset + 1] - instruction->key.begin;
        }
    }
}

void read_attribute_field_flag_escaped(struct ParseContext *context)
{
    struct AttributeFieldFlagEscaped *instruction = (struct AttributeFieldFlagEscaped *)(context->instance->instructions + context->instructions_size);

    instruction->escape_begin_operator.begin = &context->instance->input_buffer[context->input_offset];
    instruction->id = IS_ESCAPED | HAS_KEY;

    do {
        context->input_offset++;
    } while (context->instance->input_buffer[context->input_offset] == CODEPOINT_BACKTICK);

    instruction->escape_begin_operator.size = &context->instance->input_buffer[context->input_offset] - instruction->escape_begin_operator.begin;

    skip_whitespace(context);

    instruction->key.begin = &context->instance->input_buffer[context->input_offset];
    instruction->key.size = 0;

    while (1) {
        if (NEWLINE(context->instance->input_buffer[context->input_offset])) { /* TODO: EOF condition */
            read_erratic(context, PARSE_ERROR_INVALID_INSTRUCTION);
            return;
        }

        if (context->instance->input_buffer[context->input_offset] == CODEPOINT_BACKTICK) {
            instruction->escape_end_operator.begin = &context->instance->input_buffer[context->input_offset];

            do {
                context->input_offset++;

                if (&context->instance->input_buffer[context->input_offset] - instruction->escape_end_operator.begin == instruction->escape_begin_operator.size) {
                    if (instruction->key.size == 0) {
                        read_erratic(context, PARSE_ERROR_INVALID_INSTRUCTION); /* no key */
                        return;
                    }

                    instruction->escape_end_operator.size = instruction->escape_begin_operator.size;

                    goto terminated;
                }
            } while (context->instance->input_buffer[context->input_offset] == CODEPOINT_BACKTICK);
        }

        if (!WHITESPACE(context->instance->input_buffer[context->input_offset]))
            instruction->key.size = &context->instance->input_buffer[context->input_offset + 1] - instruction->key.begin;

        context->input_offset++;
    }

    terminated:

    skip_whitespace(context);

    if (NEWLINE(context->instance->input_buffer[context->input_offset])) {
        read_flag_escaped(context);
    } else if (context->instance->input_buffer[context->input_offset] == CODEPOINT_COLON) {
        read_field_escaped(context);
    } else if (context->instance->input_buffer[context->input_offset] == CODEPOINT_EQUALS_SIGN) {
        read_attribute_escaped(context);
    } else {
        /* invalid character after successful termination of escaped key */
        read_erratic(context, PARSE_ERROR_INVALID_INSTRUCTION);
    }
}

void read_attribute_escaped(struct ParseContext *context)
{
    struct AttributeEscaped *attribute = (struct AttributeEscaped *)(context->instance->instructions + context->instructions_size);
    struct Instruction *last_field_level_instruction;
    struct Instruction *last_section_level_instruction;

    attribute->operator.begin = &context->instance->input_buffer[context->input_offset];
    attribute->operator.size = 1;
    attribute->id |= IS_ATTRIBUTE;

    context->input_offset++;

    skip_whitespace(context);

    if (context->last_section_level_instruction_offset_stack[context->section_level] == NONE) {
        read_erratic(context, PARSE_ERROR_ATTRIBUTE_OUTSIDE_FIELD);
        return;
    }
    
    last_section_level_instruction = (struct Instruction *)(context->instance->instructions + context->last_section_level_instruction_offset_stack[context->section_level]);
    
    if (!(last_section_level_instruction->id & IS_FIELD)) {
        read_erratic(context, PARSE_ERROR_ATTRIBUTE_OUTSIDE_FIELD);
        return;
    }
    
    if (last_section_level_instruction->id & (HAS_VALUE | HAS_ITEMS)) {
        read_erratic(context, PARSE_ERROR_MIXED_FIELD_CONTENT);
        return;
    }
    
    last_field_level_instruction = (struct Instruction *)(context->instance->instructions + context->last_field_level_instruction_offset);

    if (last_field_level_instruction->id & IS_ATTRIBUTE) {
        last_field_level_instruction->next_sibling_offset = context->instructions_size;
    } else if (last_field_level_instruction->id & IS_ESCAPED) {
        ((struct FieldEscapedWithChildren *)last_field_level_instruction)->first_child_offset = context->instructions_size;
    } else {
        ((struct FieldWithChildren *)last_field_level_instruction)->first_child_offset = context->instructions_size;
    }
    context->last_field_level_instruction_offset = context->instructions_size;

    if (NEWLINE(context->instance->input_buffer[context->input_offset])) {
        context->instructions_size += sizeof(*attribute);
    } else {
        struct AttributeEscapedWithValue *attribute_with_value = (struct AttributeEscapedWithValue *)attribute;

        attribute_with_value->id |= HAS_VALUE;

        read_token(context, &attribute_with_value->value);

        context->instructions_size += sizeof(*attribute_with_value);
    }

    last_section_level_instruction->id |= HAS_ATTRIBUTES | HAS_CHILDREN;
    attribute->line.size = &context->instance->input_buffer[context->input_offset] - attribute->line.begin;
}

void read_comment(struct ParseContext *context)
{
    struct Comment *comment = (struct Comment *)(context->instance->instructions + context->instructions_size);

    comment->id = IS_COMMENT;
    comment->operator.begin = &context->instance->input_buffer[context->input_offset];
    comment->operator.size = 1;

    context->input_offset++;

    skip_whitespace(context);

    /* TODO: These two lines could be reviewed/improved upon still, e.g. can we derive next_comment_offset_variable_offset simpler? */
    *(size_t *)(context->instance->instructions + context->next_comment_offset_variable_offset) = context->instructions_size;
    context->next_comment_offset_variable_offset = context->instructions_size + offsetof(struct Instruction, next_sibling_offset);

    if (NEWLINE(context->instance->input_buffer[context->input_offset])) { /* TODO: EOF condition */
        context->instructions_size += sizeof(*comment);
    } else {
        struct CommentWithValue *comment_with_value = (struct CommentWithValue *)comment;

        comment_with_value->id |= HAS_VALUE;

        read_token(context, &comment_with_value->value);

        context->instructions_size += sizeof(*comment_with_value);
    }

    comment->line.size = &context->instance->input_buffer[context->input_offset] - comment->line.begin;
}

void read_embed(struct ParseContext *context)
{
    struct Embed *embed = (struct Embed *)(context->instance->instructions + context->instructions_size);

    embed->id = IS_EMBED | HAS_KEY;
    embed->operator.begin = &context->instance->input_buffer[context->input_offset];

    context->input_offset += 2;

    while (context->instance->input_buffer[context->input_offset] == CODEPOINT_DASH)
        context->input_offset++;

    embed->operator.size = &context->instance->input_buffer[context->input_offset] - embed->operator.begin;

    skip_whitespace(context);

    if (context->input_offset >= context->instance->input_size ||
        NEWLINE(context->instance->input_buffer[context->input_offset])) {
        /* TODO: Specialized error(s everywhere, or nowhere)? */
        read_erratic(context, PARSE_ERROR_INVALID_INSTRUCTION);
        return;
    }

    read_token(context, &embed->key);

    attach_element_at_current_section_level(context, sizeof(*embed));

    context->input_offset++;
    context->line_index++;

    /* TODO: We could implement and use an optimized EmbedWithValue as well (also more consistent with other representations) */
    embed->value.begin = &context->instance->input_buffer[context->input_offset];

    while (context->input_offset < context->instance->input_size) {
        embed->terminator_line.begin = &context->instance->input_buffer[context->input_offset];

        skip_whitespace(context);

        embed->terminator_operator.begin = &context->instance->input_buffer[context->input_offset];

        while (context->instance->input_buffer[context->input_offset] == CODEPOINT_DASH) {
            context->input_offset++;

            if (&context->instance->input_buffer[context->input_offset] - embed->terminator_operator.begin == embed->operator.size &&
                context->input_offset < context->instance->input_size &&
                context->instance->input_buffer[context->input_offset] != CODEPOINT_DASH) {
                UCollationResult result;

                embed->terminator_operator.size = embed->operator.size;

                skip_whitespace(context);

                embed->terminator_key.begin = &context->instance->input_buffer[context->input_offset];

                while (context->input_offset < context->instance->input_size &&
                       &context->instance->input_buffer[context->input_offset] - embed->terminator_key.begin < embed->key.size) {
                    if (NEWLINE(context->instance->input_buffer[context->input_offset]))
                        goto proceed;

                    context->input_offset++;
                }

                embed->terminator_key.size = embed->key.size;

                skip_whitespace(context);

                if (!NEWLINE(context->instance->input_buffer[context->input_offset]))
                    goto proceed;

                result = ucol_strcollUTF8(context->instance->icu_context.collator,
                                          embed->key.begin,
                                          embed->key.size,
                                          embed->terminator_key.begin,
                                          embed->terminator_key.size,
                                          &context->instance->icu_context.status);

                if (result == UCOL_EQUAL)
                    goto terminated;
            }
        }

        while (context->input_offset < context->instance->input_size &&
               !NEWLINE(context->instance->input_buffer[context->input_offset])) {
            context->input_offset++;
        }

        proceed:

        embed->value.size = &context->instance->input_buffer[context->input_offset] - embed->value.begin;

        if (context->input_offset >= context->instance->input_size) {
            context->instance->error.code = PARSE_ERROR_UNTERMINATED_EMBED;
            return;
        }

        /* Advance to start of next line */
        context->input_offset++;
        context->line_index++;
    }

    terminated:

    embed->terminator_line_number = context->line_index + 1;

    if (embed->terminator_line_number - 1 == embed->line_number) {
        embed->value.begin = NULL; /* TODO: EmbedWithValue differentiation? */
    } else {
        embed->id |= HAS_VALUE;
    }

    embed->terminator_line.size = &context->instance->input_buffer[context->input_offset] - embed->terminator_line.begin;
}

void read_erratic(struct ParseContext *context, int error_code)
{
    context->instance->error.code = error_code;
    context->instance->error.instruction_offset = context->instructions_size;

    read_unparsed(context);
}

void read_field(struct ParseContext *context)
{
    struct Field *field = (struct Field *)(context->instance->instructions + context->instructions_size);

    field->operator.begin = &context->instance->input_buffer[context->input_offset];
    field->operator.size = 1;

    context->input_offset++;

    skip_whitespace(context);

    if (NEWLINE(context->instance->input_buffer[context->input_offset])) { /* TODO: EOF condition */
        field->id |= IS_FIELD;
        ((struct FieldWithChildren *)field)->first_child_offset = NONE;
        attach_element_at_current_section_level(context, sizeof(struct FieldWithChildren));
    } else {
        field->id |= IS_FIELD | HAS_VALUE;
        read_token(context, &((struct FieldWithValue *)field)->value);
        attach_element_at_current_section_level(context, sizeof(struct FieldWithValue));
    }

    /* TODO: Rather hack-ish solution (b/c we can't use `instructions_size`, which is already increased in `attach_element_at_current_section_level`)
             See if something better presents itself later. */
    context->last_field_level_instruction_offset = (char *)field - context->instance->instructions;
}

void read_field_escaped(struct ParseContext *context)
{
    struct FieldEscaped *field = (struct FieldEscaped *)(context->instance->instructions + context->instructions_size);

    field->operator.begin = &context->instance->input_buffer[context->input_offset];
    field->operator.size = 1;

    context->input_offset++;

    skip_whitespace(context);

    if (NEWLINE(context->instance->input_buffer[context->input_offset])) { /* TODO: EOF condition */
        field->id |= IS_FIELD;
        ((struct FieldEscapedWithChildren *)field)->first_child_offset = NONE;
        attach_element_at_current_section_level(context, sizeof(struct FieldEscapedWithChildren));
    } else {
        field->id |= IS_FIELD | HAS_VALUE;
        read_token(context, &((struct FieldEscapedWithValue *)field)->value);
        attach_element_at_current_section_level(context, sizeof(struct FieldEscapedWithValue));
    }

    /* TODO: Rather hack-ish solution (b/c we can't use `instructions_size`, which is already increased in `attach_element_at_current_section_level`)
             See if something better presents itself later. */
    context->last_field_level_instruction_offset = (char *)field - context->instance->instructions;
}

void read_flag(struct ParseContext *context)
{
    struct Instruction *flag = (struct Instruction *)(context->instance->instructions + context->instructions_size);
    flag->id |= IS_FLAG;
    attach_element_at_current_section_level(context, sizeof(struct Flag));
}

void read_flag_escaped(struct ParseContext *context)
{
    struct Instruction *flag = (struct Instruction *)(context->instance->instructions + context->instructions_size);
    flag->id |= IS_FLAG;
    attach_element_at_current_section_level(context, sizeof(struct FlagEscaped));
}

void read_item(struct ParseContext *context)
{
    struct Item *item = (struct Item *)(context->instance->instructions + context->instructions_size);
    struct Instruction *last_field_level_instruction;
    struct Instruction *last_section_level_instruction;

    if (context->last_section_level_instruction_offset_stack[context->section_level] == NONE) {
        read_erratic(context, PARSE_ERROR_ITEM_OUTSIDE_FIELD);
        return;
    }
    
    last_section_level_instruction = (struct Instruction *)(context->instance->instructions + context->last_section_level_instruction_offset_stack[context->section_level]);
    
    if (!(last_section_level_instruction->id & IS_FIELD)) {
        read_erratic(context, PARSE_ERROR_ITEM_OUTSIDE_FIELD);
        return;
    }

    if (last_section_level_instruction->id & (HAS_VALUE | HAS_ATTRIBUTES)) {
        read_erratic(context, PARSE_ERROR_MIXED_FIELD_CONTENT);
        return;
    }

    item->id = IS_ITEM;
    item->operator.begin = &context->instance->input_buffer[context->input_offset];
    item->operator.size = 1;

    context->input_offset++;

    skip_whitespace(context);
    
    last_field_level_instruction = (struct Instruction *)(context->instance->instructions + context->last_field_level_instruction_offset);

    if (last_field_level_instruction->id & IS_ITEM) {
        last_field_level_instruction->next_sibling_offset = context->instructions_size;
    } else if (last_field_level_instruction->id & IS_ESCAPED) {
        ((struct FieldEscapedWithChildren *)last_field_level_instruction)->first_child_offset = context->instructions_size;
    } else {
        ((struct FieldWithChildren *)last_field_level_instruction)->first_child_offset = context->instructions_size;
    }
    context->last_field_level_instruction_offset = context->instructions_size;

    last_section_level_instruction->id |= HAS_CHILDREN | HAS_ITEMS;

    if (NEWLINE(context->instance->input_buffer[context->input_offset])) {
        context->instructions_size += sizeof(*item);
    } else {
        struct ItemWithValue *item_with_value = (struct ItemWithValue *)item;

        item_with_value->id |= HAS_VALUE;

        read_token(context, &item_with_value->value);

        context->instructions_size += sizeof(*item_with_value);
    }

    item->line.size = &context->instance->input_buffer[context->input_offset] - item->line.begin;
}

void read_section(struct ParseContext *context)
{
    struct Section *section = (struct Section *)(context->instance->instructions + context->instructions_size);

    section->first_child_offset = NONE;
    section->id = HAS_KEY | IS_SECTION;
    section->operator.begin = &context->instance->input_buffer[context->input_offset];

    context->input_offset++;

    while (context->input_offset < context->instance->input_size &&
           context->instance->input_buffer[context->input_offset] == CODEPOINT_HASH) {
        if (&context->instance->input_buffer[context->input_offset] - section->operator.begin > context->section_level + 1) {
            /* TODO: Try to parse the rest of the instruction anyway? (for reporting purposes) */
            read_erratic(context, PARSE_ERROR_SECTION_HIERARCHY_LAYER_SKIP);
            return;
        }

        context->input_offset++;
    }

    section->operator.size = &context->instance->input_buffer[context->input_offset] - section->operator.begin;

    context->section_level = section->operator.size  - 1;

    skip_whitespace(context);

    if (NEWLINE(context->instance->input_buffer[context->input_offset])) {
        read_erratic(context, PARSE_ERROR_SECTION_WITHOUT_KEY);
        return;
    }

    section->key.begin = &context->instance->input_buffer[context->input_offset];

    while (1) {
        context->input_offset++;

        if (context->input_offset >= context->instance->input_size ||
            NEWLINE(context->instance->input_buffer[context->input_offset])) {
            attach_element_at_current_section_level(context, sizeof(*section));
            context->last_section_level_instruction_offset_stack[++context->section_level] = NONE;
            break;
        } else if (!WHITESPACE(context->instance->input_buffer[context->input_offset])) {
            section->key.size = &context->instance->input_buffer[context->input_offset + 1] - section->key.begin;
        }
    }
}

void read_token(struct ParseContext *context, struct Token *token)
{
    token->begin = &context->instance->input_buffer[context->input_offset];

    while (context->input_offset < context->instance->input_size &&
           !NEWLINE(context->instance->input_buffer[context->input_offset])) {
        if (!WHITESPACE(context->instance->input_buffer[context->input_offset]))
            token->size = &context->instance->input_buffer[context->input_offset + 1] - token->begin;

        context->input_offset++;
    }
}

void read_unparsed(struct ParseContext *context)
{
    struct Instruction *instruction = (struct Instruction *)(context->instance->instructions + context->instructions_size);

    instruction->id = IS_UNPARSED;

    while (context->input_offset < context->instance->input_size &&
           !NEWLINE(context->instance->input_buffer[context->input_offset]))
        context->input_offset++;

    instruction->line.size = &context->instance->input_buffer[context->input_offset] - instruction->line.begin;

    *(size_t *)(context->instance->instructions + context->next_unparsed_instruction_offset_variable_offset) = context->instructions_size;
    context->next_unparsed_instruction_offset_variable_offset = context->instructions_size + offsetof(struct Instruction, next_sibling_offset);
    context->instructions_size += sizeof(*instruction);
}

void skip_whitespace(struct ParseContext *context)
{
    while (context->input_offset < context->instance->input_size &&
           WHITESPACE(context->instance->input_buffer[context->input_offset])) {
        context->input_offset++;
    }
}
