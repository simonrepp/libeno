# Changelog

This project follows semantic versioning.

## Unreleased

- Remove preliminary copies implementation and all related parts `87013af`
- Handle zero length input in stream read code `f1ae8e3`
- Support arbitrarily large documents up to 4GB size `7d888a6`
- Enable parse error printing to a specified stream or to memory `7f25d60`

  ```c
  // This one goes away (no way to print to memory, hardcoded to stderr)
  void eno_report_error(ENOIterator *iterator);

  // These two replace it (print to memory or any stream you like)
  void eno_print_error_to_memory(ENOIterator *iterator, char *buffer, size_t size);
  void eno_print_error_to_stream(ENOIterator *iterator, FILE *stream);
  ```
  
- Resolve critical issue when parsing an unterminated escaped key `c85e573`

## 0.1.0

- Initial versioned release
