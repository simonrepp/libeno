#ifndef ENO_CODEPOINTS_H
#define ENO_CODEPOINTS_H

#define CODEPOINT_BACKTICK 0x60
#define CODEPOINT_COLON 0x3A
#define CODEPOINT_DASH 0x2D
#define CODEPOINT_EQUALS_SIGN 0x3D
#define CODEPOINT_GREATER_THAN_SIGN 0x3E
#define CODEPOINT_HASH 0x23
#define CODEPOINT_HORIZONTAL_TAB 0x9
#define CODEPOINT_SPACE 0x20

#endif
