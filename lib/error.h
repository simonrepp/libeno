#ifndef ENO_ERROR_H
#define ENO_ERROR_H

#include "eno.h"

#define PARSE_ERROR_ATTRIBUTE_OUTSIDE_FIELD -1
#define PARSE_ERROR_INVALID_INSTRUCTION -3
#define PARSE_ERROR_ITEM_OUTSIDE_FIELD -4
#define PARSE_ERROR_MIXED_FIELD_CONTENT -5
#define PARSE_ERROR_SECTION_HIERARCHY_LAYER_SKIP -6
#define PARSE_ERROR_SECTION_WITHOUT_KEY -7
#define PARSE_ERROR_UNTERMINATED_EMBED -8
#define READ_ERROR_OUT_OF_MEMORY -9
#define READ_ERROR_STREAM_ERROR -10

void print_error(
    ENOIterator *opaque_iterator,
    void (*print_func)(char *string, void *varying_a, void *varying_b),
    void *varying_a,
    void *varying_b
);
void print_to_memory(char *string, void *varying_a, void *varying_b);
void print_to_stream(char *string, void *varying_a, void *varying_b);

#endif
