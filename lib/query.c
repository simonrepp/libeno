#include "eno.h"
#include "instructions.h"
#include "iterate.h"

unsigned int eno_has_attributes(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & HAS_ATTRIBUTES;
}

/*
 * @return 1 if the iterated element contains attributes or items (in the case of a field)
 *         or arbitrary elements (in the case of a section or the document itself), otherwise
 *         returns 0.
 */
unsigned int eno_has_children(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & HAS_CHILDREN;
}

unsigned int eno_has_items(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & HAS_ITEMS;
}

unsigned int eno_has_key(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & HAS_KEY;
}

unsigned int eno_has_value(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & HAS_VALUE;
}

/**
 * @returns 0 if the iterator is used up or the iterated element is not an attribute, otherwise 1
 */
unsigned int eno_is_attribute(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & IS_ATTRIBUTE;
}

/**
 * @returns 0 if the iterator is used up or the iterated element is not the document, otherwise 1
 */
unsigned int eno_is_document(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & IS_DOCUMENT;
}

/**
 * @returns 0 if the iterator is used up or the iterated element is not an embed, otherwise 1
 */
unsigned int eno_is_embed(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & IS_EMBED;
}

/**
 * @returns 0 if the iterator is used up or the iterated element is not a field, otherwise 1
 */
unsigned int eno_is_field(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & IS_FIELD;
}

/**
 * @returns 0 if the iterator is used up or the iterated element is not a flag, otherwise 1
 */
unsigned int eno_is_flag(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & IS_FLAG;
}

/**
 * @returns 0 if the iterator is used up or the iterated element is not an item, otherwise 1
 */
unsigned int eno_is_item(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & IS_ITEM;
}

/**
 * @returns 0 if the iterator is used up or the iterated element is not a section, otherwise 1
 */
unsigned int eno_is_section(ENOIterator *opaque_iterator)
{
    struct Instruction *instruction = ((struct Iterator *)opaque_iterator)->current_instruction;
    return instruction != NULL && instruction->id & IS_SECTION;
}
