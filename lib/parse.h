#ifndef ENO_PARSE_H
#define ENO_PARSE_H

#include "eno.h"
#include "instance.h"
#include "instructions.h"
#include "error.h"

/* TODO: Implement an additional dynamic one on the heap when we overflow the static one */
#define STATIC_LAST_NEXT_ELEMENT_POINTER_STACK_SIZE 16

/**
 * Line feed, vertical tab, form feed, carriage return,
 * next line, line separator, paragraph separator
 */
#define NEWLINE(c) ( \
    ((c) >= 0xa && (c) <= 0xd) || \
    (c) == 0x85 || (c) == 0x2028 || (c) == 0x2029 ) /* TODO: Why do we handle vertical tab, etc. (?) */

#define WHITESPACE(c) ( \
    (c) == CODEPOINT_SPACE || \
    (c) == CODEPOINT_HORIZONTAL_TAB )

struct ParseContext {
    size_t input_offset; /* refers to utf-8 codeunits (= bytes) */
    struct Instance *instance;
    size_t instructions_capacity;
    size_t instructions_size;
    size_t last_field_level_instruction_offset;
    size_t last_section_level_instruction_offset_stack[STATIC_LAST_NEXT_ELEMENT_POINTER_STACK_SIZE];
    size_t last_section_level_instruction_offset_stack_size;
    /** Byte offset from instance->instructions to (initially) document->first_comment_offset or (later) some_comment->next_sibling_offset */
    size_t next_comment_offset_variable_offset;
    /** Byte offset from instance->instructions to (initially) document->first_unparsed_instruction_offset or (later) some_unparsed_instruction->next_sibling_offset */
    size_t next_unparsed_instruction_offset_variable_offset;
    unsigned long int line_index;
    unsigned int section_level;
};

void attach_element_at_current_section_level(struct ParseContext *context, size_t size);
void ensure_instructions_capacity(struct ParseContext *context);
void parse_document(struct Instance *instance);
unsigned int eno_parse_internal(ENOIterator *opaque_iterator, FILE *stream, char *buffer, size_t size);
void read_attribute(struct ParseContext *context);
void read_attribute_field_flag(struct ParseContext *context);
void read_attribute_field_flag_escaped(struct ParseContext *context);
void read_attribute_escaped(struct ParseContext *context);
void read_comment(struct ParseContext *context);
void read_embed(struct ParseContext *context);
void read_erratic(struct ParseContext *context, int error_code);
void read_field(struct ParseContext *context);
void read_field_escaped(struct ParseContext *context);
void read_flag(struct ParseContext *context);
void read_flag_escaped(struct ParseContext *context);
void read_item(struct ParseContext *context);
void read_section(struct ParseContext *context);
void read_token(struct ParseContext *context, struct Token *token);
void read_unparsed(struct ParseContext *context);
void skip_whitespace(struct ParseContext *context);

#endif
