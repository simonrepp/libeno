#ifndef ENO_HELPERS_H
#define ENO_HELPERS_H

#include <stdio.h>
#include <string.h>

#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_RESET "\x1b[0m"

#define DIFF_LINE_BUFFER_SIZE 512

int diff(FILE *file_expected, FILE *file_received);

#endif