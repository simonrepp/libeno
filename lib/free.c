#include <stdlib.h>

#include "eno.h"
#include "iterate.h"
#include "parse.h"

/**
 * Frees all resources that were allocated for the document.
 * This invalidates absolutely all pointers that were
 * obtained (keys, values, etc.) and anything you plan on
 * using further must already have been copied at this point.
 */
void eno_free_document(ENOIterator *opaque_iterator)
{
    struct Instance *instance = ((struct Iterator *)opaque_iterator)->instance;

    ucol_close(instance->icu_context.collator);

    free(instance->input_buffer);
    free(instance->instructions);
    free(instance);
}
