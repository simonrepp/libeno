/*
 * Note: Everywhere an instruction (or an empty line as a special case) is printed
 *       the context->line_number is increased by 1, past that instruction's line
 */

#include <stdio.h>

#include "../lib/iterate.h"
#include "serialize_internal_ast.h"

void serialize_internal_ast_attribute(struct SerializeInternalAstContext *context, size_t offset)
{
    struct Attribute *attribute = (struct Attribute *)(context->instance->instructions + offset);
    
    serialize_internal_ast_preceding_idle_instructions(context, attribute->line_number);

    if (attribute->id & HAS_VALUE) {
        struct AttributeWithValue *attribute_with_value = (struct AttributeWithValue *)attribute;

        fprintf(context->out_stream,
                "%.*s·%.*s·%.*s·=·%.*s·%.*s·%.*s·␊\n",
                (unsigned int)(attribute_with_value->key.begin - attribute_with_value->line.begin),
                attribute_with_value->line.begin,
                attribute_with_value->key.size,
                attribute_with_value->key.begin,
                (unsigned int)(attribute_with_value->operator.begin - attribute_with_value->key.begin - attribute_with_value->key.size),
                attribute_with_value->key.begin + attribute_with_value->key.size,
                (unsigned int)(attribute_with_value->value.begin - attribute_with_value->operator.begin - attribute_with_value->operator.size),
                attribute_with_value->operator.begin + attribute_with_value->operator.size,
                attribute_with_value->value.size,
                attribute_with_value->value.begin,
                (unsigned int)(attribute_with_value->line.begin + attribute_with_value->line.size - attribute_with_value->value.begin - attribute_with_value->value.size),
                attribute_with_value->value.begin + attribute_with_value->value.size);
    } else {
        fprintf(context->out_stream,
                "%.*s·%.*s·%.*s·=·%.*s·␊\n",
                (unsigned int)(attribute->key.begin - attribute->line.begin),
                attribute->line.begin,
                attribute->key.size,
                attribute->key.begin,
                (unsigned int)(attribute->operator.begin - attribute->key.begin - attribute->key.size),
                attribute->key.begin + attribute->key.size,
                (unsigned int)(attribute->line.begin + attribute->line.size - attribute->operator.begin - attribute->operator.size),
                attribute->operator.begin + attribute->operator.size);
    }

    context->line_number = attribute->line_number + 1;
}

void serialize_internal_ast_attribute_escaped(struct SerializeInternalAstContext *context, size_t offset)
{
    struct AttributeEscaped *attribute = (struct AttributeEscaped *)(context->instance->instructions + offset);
    
    serialize_internal_ast_preceding_idle_instructions(context, attribute->line_number);

    if (attribute->id & HAS_VALUE) {
        struct AttributeEscapedWithValue *attribute_with_value = (struct AttributeEscapedWithValue *)attribute;

        fprintf(context->out_stream,
                "%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·=·%.*s·%.*s·%.*s·␊\n",
                (unsigned int)(attribute_with_value->escape_begin_operator.begin - attribute_with_value->line.begin),
                attribute_with_value->line.begin,
                attribute_with_value->escape_begin_operator.size,
                attribute_with_value->escape_begin_operator.begin,
                (unsigned int)(attribute_with_value->key.begin - attribute_with_value->escape_begin_operator.begin - attribute_with_value->escape_begin_operator.size),
                attribute_with_value->escape_begin_operator.begin + attribute_with_value->escape_begin_operator.size,
                attribute_with_value->key.size,
                attribute_with_value->key.begin,
                (unsigned int)(attribute_with_value->escape_end_operator.begin - attribute_with_value->key.begin - attribute_with_value->key.size),
                attribute_with_value->key.begin + attribute_with_value->key.size,
                attribute_with_value->escape_end_operator.size,
                attribute_with_value->escape_end_operator.begin,
                (unsigned int)(attribute_with_value->operator.begin - attribute_with_value->escape_end_operator.begin - attribute_with_value->escape_end_operator.size),
                attribute_with_value->escape_end_operator.begin + attribute_with_value->escape_end_operator.size,
                (unsigned int)(attribute_with_value->value.begin - attribute_with_value->operator.begin - attribute_with_value->operator.size),
                attribute_with_value->operator.begin + attribute_with_value->operator.size,
                attribute_with_value->value.size,
                attribute_with_value->value.begin,
                (unsigned int)(attribute_with_value->line.begin + attribute_with_value->line.size - attribute_with_value->value.begin - attribute_with_value->value.size),
                attribute_with_value->value.begin + attribute_with_value->value.size);
    } else {
        fprintf(context->out_stream,
                "%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·=·%.*s·␊\n",
                (unsigned int)(attribute->escape_begin_operator.begin - attribute->line.begin),
                attribute->line.begin,
                attribute->escape_begin_operator.size,
                attribute->escape_begin_operator.begin,
                (unsigned int)(attribute->key.begin - attribute->escape_begin_operator.begin - attribute->escape_begin_operator.size),
                attribute->escape_begin_operator.begin + attribute->escape_begin_operator.size,
                attribute->key.size,
                attribute->key.begin,
                (unsigned int)(attribute->escape_end_operator.begin - attribute->key.begin - attribute->key.size),
                attribute->key.begin + attribute->key.size,
                attribute->escape_end_operator.size,
                attribute->escape_end_operator.begin,
                (unsigned int)(attribute->operator.begin - attribute->escape_end_operator.begin - attribute->escape_end_operator.size),
                attribute->escape_end_operator.begin + attribute->escape_end_operator.size,
                (unsigned int)(attribute->line.begin + attribute->line.size - attribute->operator.begin - attribute->operator.size),
                attribute->operator.begin + attribute->operator.size);
    }

    context->line_number = attribute->line_number + 1;
}

void serialize_internal_ast_comment(struct SerializeInternalAstContext *context, size_t offset)
{
    struct Comment *comment = (struct Comment *)(context->instance->instructions + offset);
    
    if (comment->id & HAS_VALUE) {
        struct CommentWithValue *comment_with_value = (struct CommentWithValue *)comment;

        fprintf(context->out_stream,
                "%.*s·>·%.*s·%.*s·%.*s·␊\n",
                (unsigned int)(comment_with_value->operator.begin - comment_with_value->line.begin),
                comment_with_value->line.begin,
                (unsigned int)(comment_with_value->value.begin - comment_with_value->operator.begin - 1),
                comment_with_value->operator.begin + comment_with_value->operator.size,
                comment_with_value->value.size,
                comment_with_value->value.begin,
                (unsigned int)(comment_with_value->line.begin + comment_with_value->line.size - comment_with_value->value.begin - comment_with_value->value.size),
                comment_with_value->value.begin + comment_with_value->value.size);
    } else {
        fprintf(context->out_stream,
                "%.*s·>·%.*s·␊\n",
                (unsigned int)(comment->operator.begin - comment->line.begin),
                comment->line.begin,
                (unsigned int)(comment->line.begin + comment->line.size - comment->operator.begin - comment->operator.size),
                comment->operator.begin + comment->operator.size);
    }

    context->line_number = comment->line_number + 1;
}

/**
 * Reconstructs the original document using only private library functions
 * and the internal AST obtained from the document during parsing.
 * In other words the document is re-generated 1:1 as it was read.
 */
void serialize_internal_ast_document(ENOIterator *opaque_iterator, FILE *out_stream)
{
    struct SerializeInternalAstContext context;
    struct Instance *instance = ((struct Iterator *)opaque_iterator)->instance;
    struct Document *document = (struct Document *)instance->instructions;

    context.instance = instance;
    context.line_number = 1;
    context.next_comment_offset = document->first_comment_offset;
    context.out_stream = out_stream;

    serialize_internal_ast_elements(&context, 0);
    serialize_internal_ast_preceding_idle_instructions(&context, instance->input_linecount - 1);
}

void serialize_internal_ast_elements(struct SerializeInternalAstContext *context, size_t offset)
{
    while (offset != NONE) {
        struct Instruction *instruction = (struct Instruction *)(context->instance->instructions + offset);
        
        if (instruction->id & IS_ESCAPED) {
            if(instruction->id & IS_FLAG) {
                serialize_internal_ast_flag_escaped(context, offset);
            } else /* if(instruction->id & IS_FIELD) */ {
                serialize_internal_ast_field_escaped(context, offset);
            }
        } else if(instruction->id & IS_EMBED) {
            serialize_internal_ast_embed(context, offset);
        } else if(instruction->id & IS_FIELD) {
            serialize_internal_ast_field(context, offset);
        } else if(instruction->id & IS_FLAG) {
            serialize_internal_ast_flag(context, offset);
        } else if(instruction->id & IS_SECTION) {
            serialize_internal_ast_section(context, offset);
        }

        offset = instruction->next_sibling_offset;
    }
}

void serialize_internal_ast_embed(struct SerializeInternalAstContext *context, size_t offset)
{
    struct Embed *embed = (struct Embed *)(context->instance->instructions + offset);
    
    serialize_internal_ast_preceding_idle_instructions(context, embed->line_number);

    fprintf(context->out_stream,
            "%.*s·%.*s·%.*s·%.*s·%.*s·␊\n",
            (unsigned int)(embed->operator.begin - embed->line.begin),
            embed->line.begin,
            embed->operator.size,
            embed->operator.begin,
            (unsigned int)(embed->key.begin - embed->operator.begin - embed->operator.size),
            embed->operator.begin + embed->operator.size,
            embed->key.size,
            embed->key.begin,
            (unsigned int)(embed->line.begin + embed->line.size - embed->key.begin - embed->key.size),
            embed->key.begin + embed->key.size);

    if (embed->id & HAS_VALUE)
        fprintf(context->out_stream,
                "%.*s\n",
                embed->value.size,
                embed->value.begin);

    fprintf(context->out_stream,
            "%.*s·%.*s·%.*s·%.*s·%.*s·␊\n",
            (unsigned int)(embed->terminator_operator.begin - embed->terminator_line.begin),
            embed->terminator_line.begin,
            embed->terminator_operator.size,
            embed->terminator_operator.begin,
            (unsigned int)(embed->terminator_key.begin - embed->terminator_operator.begin - embed->terminator_operator.size),
            embed->terminator_operator.begin + embed->terminator_operator.size,
            embed->terminator_key.size,
            embed->terminator_key.begin,
            (unsigned int)(embed->terminator_line.begin + embed->terminator_line.size - embed->terminator_key.begin - embed->terminator_key.size),
            embed->terminator_key.begin + embed->terminator_key.size);

    context->line_number = embed->terminator_line_number + 1;
}

void serialize_internal_ast_empty_line(struct SerializeInternalAstContext *context)
{
    fputc('\n', context->out_stream);
    context->line_number++;
}

void serialize_internal_ast_field(struct SerializeInternalAstContext *context, size_t offset)
{
    struct Field *field = (struct Field *)(context->instance->instructions + offset);
    
    serialize_internal_ast_preceding_idle_instructions(context, field->line_number);

    if (field->id & HAS_VALUE) {
        struct FieldWithValue *field_with_value = (struct FieldWithValue *)field;

        fprintf(context->out_stream,
                "%.*s·%.*s·%.*s·:·%.*s·%.*s·%.*s·␊\n",
                (unsigned int)(field_with_value->key.begin - field_with_value->line.begin),
                field_with_value->line.begin,
                field_with_value->key.size,
                field_with_value->key.begin,
                (unsigned int)(field_with_value->operator.begin - field_with_value->key.begin - field_with_value->key.size),
                field_with_value->key.begin + field_with_value->key.size,
                (unsigned int)(field_with_value->value.begin - field_with_value->operator.begin - field_with_value->operator.size),
                field_with_value->operator.begin + field_with_value->operator.size,
                field_with_value->value.size,
                field_with_value->value.begin,
                (unsigned int)(field_with_value->line.begin + field_with_value->line.size - field_with_value->value.begin - field_with_value->value.size),
                field_with_value->value.begin + field_with_value->value.size);
    } else {
        fprintf(context->out_stream,
                "%.*s·%.*s·%.*s·:·%.*s·␊\n",
                (unsigned int)(field->key.begin - field->line.begin),
                field->line.begin,
                field->key.size,
                field->key.begin,
                (unsigned int)(field->operator.begin - field->key.begin - field->key.size),
                field->key.begin + field->key.size,
                (unsigned int)(field->line.begin + field->line.size - field->operator.begin - field->operator.size),
                field->operator.begin + field->operator.size);
    }

    context->line_number = field->line_number + 1;

    if (field->id & HAS_CHILDREN) {
        serialize_internal_ast_field_children(context, ((struct FieldWithChildren *)field)->first_child_offset);
    }
}

void serialize_internal_ast_field_children(struct SerializeInternalAstContext *context, size_t offset)
{
    while (offset != NONE) {
        struct Instruction *child = (struct Instruction *)(context->instance->instructions + offset);
        
        if (child->id & IS_ATTRIBUTE) {
            if (child->id & IS_ESCAPED) {
                serialize_internal_ast_attribute_escaped(context, offset);
            } else {
                serialize_internal_ast_attribute(context, offset);
            }
        } else /* if (child->id & IS_ITEM) */ {
            serialize_internal_ast_item(context, offset);
        }
        offset = child->next_sibling_offset;
    }
}

void serialize_internal_ast_field_escaped(struct SerializeInternalAstContext *context, size_t offset)
{
    struct FieldEscaped *field = (struct FieldEscaped *)(context->instance->instructions + offset);
    
    serialize_internal_ast_preceding_idle_instructions(context, field->line_number);

    if (field->id & HAS_VALUE) {
        struct FieldEscapedWithValue *field_with_value = (struct FieldEscapedWithValue *)field;

        fprintf(context->out_stream,
                "%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·:·%.*s·%.*s·%.*s·␊\n",
                (unsigned int)(field_with_value->escape_begin_operator.begin - field_with_value->line.begin),
                field_with_value->line.begin,
                field_with_value->escape_begin_operator.size,
                field_with_value->escape_begin_operator.begin,
                (unsigned int)(field_with_value->key.begin - field_with_value->escape_begin_operator.begin - field_with_value->escape_begin_operator.size),
                field_with_value->escape_begin_operator.begin + field_with_value->escape_begin_operator.size,
                field_with_value->key.size,
                field_with_value->key.begin,
                (unsigned int)(field_with_value->escape_end_operator.begin - field_with_value->key.begin - field_with_value->key.size),
                field_with_value->key.begin + field_with_value->key.size,
                field_with_value->escape_end_operator.size,
                field_with_value->escape_end_operator.begin,
                (unsigned int)(field_with_value->operator.begin - field_with_value->escape_end_operator.begin - field_with_value->escape_end_operator.size),
                field_with_value->escape_end_operator.begin + field_with_value->escape_end_operator.size,
                (unsigned int)(field_with_value->value.begin - field_with_value->operator.begin - field_with_value->operator.size),
                field_with_value->operator.begin + field_with_value->operator.size,
                field_with_value->value.size,
                field_with_value->value.begin,
                (unsigned int)(field_with_value->line.begin + field_with_value->line.size - field_with_value->value.begin - field_with_value->value.size),
                field_with_value->value.begin + field_with_value->value.size);
    } else {
        fprintf(context->out_stream,
                "%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·:·%.*s·␊\n",
                (unsigned int)(field->escape_begin_operator.begin - field->line.begin),
                field->line.begin,
                field->escape_begin_operator.size,
                field->escape_begin_operator.begin,
                (unsigned int)(field->key.begin - field->escape_begin_operator.begin - field->escape_begin_operator.size),
                field->escape_begin_operator.begin + field->escape_begin_operator.size,
                field->key.size,
                field->key.begin,
                (unsigned int)(field->escape_end_operator.begin - field->key.begin - field->key.size),
                field->key.begin + field->key.size,
                field->escape_end_operator.size,
                field->escape_end_operator.begin,
                (unsigned int)(field->operator.begin - field->escape_end_operator.begin - field->escape_end_operator.size),
                field->escape_end_operator.begin + field->escape_end_operator.size,
                (unsigned int)(field->line.begin + field->line.size - field->operator.begin - field->operator.size),
                field->operator.begin + field->operator.size);
    }


    context->line_number = field->line_number + 1;

    if (field->id & HAS_CHILDREN) {
        serialize_internal_ast_field_children(context, ((struct FieldEscapedWithChildren *)field)->first_child_offset);
    }
}

void serialize_internal_ast_flag(struct SerializeInternalAstContext *context, size_t offset)
{
    struct Flag *flag = (struct Flag *)(context->instance->instructions + offset);
    
    serialize_internal_ast_preceding_idle_instructions(context, flag->line_number);

    fprintf(context->out_stream,
            "%.*s·%.*s·%.*s·␊\n",
            (unsigned int)(flag->key.begin - flag->line.begin),
            flag->line.begin,
            flag->key.size,
            flag->key.begin,
            (unsigned int)(flag->line.begin + flag->line.size - flag->key.begin - flag->key.size),
            flag->key.begin + flag->key.size);

    context->line_number = flag->line_number + 1;
}

void serialize_internal_ast_flag_escaped(struct SerializeInternalAstContext *context, size_t offset)
{
    struct FlagEscaped *flag = (struct FlagEscaped *)(context->instance->instructions + offset);
    
    serialize_internal_ast_preceding_idle_instructions(context, flag->line_number);

    fprintf(context->out_stream,
            "%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·%.*s·␊\n",
            (unsigned int)(flag->escape_begin_operator.begin - flag->line.begin),
            flag->line.begin,
            flag->escape_begin_operator.size,
            flag->escape_begin_operator.begin,
            (unsigned int)(flag->key.begin - flag->escape_begin_operator.begin - flag->escape_begin_operator.size),
            flag->escape_begin_operator.begin + flag->escape_begin_operator.size,
            flag->key.size,
            flag->key.begin,
            (unsigned int)(flag->escape_end_operator.begin - flag->key.begin - flag->key.size),
            flag->key.begin + flag->key.size,
            flag->escape_end_operator.size,
            flag->escape_end_operator.begin,
            (unsigned int)(flag->line.begin + flag->line.size - flag->escape_end_operator.begin - flag->escape_end_operator.size),
            flag->escape_end_operator.begin + flag->escape_end_operator.size);

    context->line_number = flag->line_number + 1;
}

void serialize_internal_ast_item(struct SerializeInternalAstContext *context, size_t offset)
{
    struct Item *item = (struct Item *)(context->instance->instructions + offset);
    
    serialize_internal_ast_preceding_idle_instructions(context, item->line_number);

    if (item->id & HAS_VALUE) {
        struct ItemWithValue *item_with_value = (struct ItemWithValue *)item;

        fprintf(context->out_stream,
                "%.*s·-·%.*s·%.*s·%.*s·␊\n",
                (unsigned int)(item_with_value->operator.begin - item_with_value->line.begin),
                item_with_value->line.begin,
                (unsigned int)(item_with_value->value.begin - item_with_value->operator.begin - 1),
                item_with_value->operator.begin + item_with_value->operator.size,
                item_with_value->value.size,
                item_with_value->value.begin,
                (unsigned int)(item_with_value->line.begin + item_with_value->line.size - item_with_value->value.begin - item_with_value->value.size),
                item_with_value->value.begin + item_with_value->value.size);
    } else {
        fprintf(context->out_stream,
                "%.*s·-·%.*s·␊\n",
                (unsigned int)(item->operator.begin - item->line.begin),
                item->line.begin,
                (unsigned int)(item->line.begin + item->line.size - item->operator.begin - item->operator.size),
                item->operator.begin + item->operator.size);
    }

    context->line_number = item->line_number + 1;
}

void serialize_internal_ast_preceding_idle_instructions(struct SerializeInternalAstContext *context, unsigned long int before_line_number)
{
    while (context->line_number < before_line_number) {
        if (context->next_comment_offset != NONE) {
            struct Comment *comment = (struct Comment *)(context->instance->instructions + context->next_comment_offset);
            
            if (comment->line_number == context->line_number) {
                serialize_internal_ast_comment(context, context->next_comment_offset);
                context->next_comment_offset = comment->next_sibling_offset;
                continue;
            }
        }
        
        serialize_internal_ast_empty_line(context);
    }
}

void serialize_internal_ast_section(struct SerializeInternalAstContext *context, size_t offset)
{
    struct Section *section = (struct Section *)(context->instance->instructions + offset);
    
    serialize_internal_ast_preceding_idle_instructions(context, section->line_number);

    fprintf(context->out_stream,
            "%.*s·%.*s·%.*s·%.*s·%.*s·␊\n",
            (unsigned int)(section->operator.begin - section->line.begin),
            section->line.begin,
            section->operator.size,
            section->operator.begin,
            (unsigned int)(section->key.begin - section->operator.begin - section->operator.size),
            section->operator.begin + section->operator.size,
            section->key.size,
            section->key.begin,
            (unsigned int)(section->line.begin + section->line.size - section->key.begin - section->key.size),
            section->key.begin + section->key.size);

    context->line_number = section->line_number + 1;

    serialize_internal_ast_elements(context, section->first_child_offset);
}