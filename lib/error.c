#include <string.h>

#include "error.h"
#include "eno.h"
#include "iterate.h"
#include "parse.h"

/**
 * @brief Prints the failure cause for eno_parse_* to the specified buffer
 * @param opaque_iterator The iterator that was passed to eno_parse_*
 * @param buffer Pointer to the buffer to which the error shall be printed
 * @param size Size of the buffer (currently it is advised to provide at least ENO_MAX_ERROR_SIZE bytes space)
 */
void eno_print_error_to_memory(ENOIterator *opaque_iterator, char *buffer, size_t size)
{
    print_error(opaque_iterator, &print_to_memory, buffer, &size);
}

/**
 * @brief Prints the failure cause for eno_parse_* to the passed stream
 * @param opaque_iterator The iterator that was passed to eno_parse_*
 * @param stream The stream to which the error shall be printed
 */
void eno_print_error_to_stream(ENOIterator *opaque_iterator, FILE *stream)
{
    print_error(opaque_iterator, &print_to_stream, stream, NULL);
}

void print_error(
    ENOIterator *opaque_iterator,
    void (*print_func)(char *string, void *varying_a, void *varying_b),
    void *varying_a,
    void *varying_b
)
{
    struct Instance *instance = ((struct Iterator *)opaque_iterator)->instance;

    switch (instance->error.code) {
        /* TODO: Implement all types, follow up with how this is actually used */

        case PARSE_ERROR_ATTRIBUTE_OUTSIDE_FIELD:
            (*print_func)("PARSE_ERROR_ATTRIBUTE_OUTSIDE_FIELD\n", varying_a, varying_b);
            return;
        case PARSE_ERROR_INVALID_INSTRUCTION:
            (*print_func)("PARSE_ERROR_INVALID_INSTRUCTION\n", varying_a, varying_b);
            return;
        case PARSE_ERROR_ITEM_OUTSIDE_FIELD:
            (*print_func)("PARSE_ERROR_ITEM_OUTSIDE_FIELD\n", varying_a, varying_b);
            return;
        case PARSE_ERROR_MIXED_FIELD_CONTENT:
            (*print_func)("PARSE_ERROR_MIXED_FIELD_CONTENT\n", varying_a, varying_b);
            return;
        case PARSE_ERROR_SECTION_HIERARCHY_LAYER_SKIP:
            (*print_func)("PARSE_ERROR_SECTION_HIERARCHY_LAYER_SKIP\n", varying_a, varying_b);
            return;
        case PARSE_ERROR_SECTION_WITHOUT_KEY:
            (*print_func)("PARSE_ERROR_SECTION_WITHOUT_KEY\n", varying_a, varying_b);
            return;
        case PARSE_ERROR_UNTERMINATED_EMBED:
            (*print_func)("PARSE_ERROR_UNTERMINATED_EMBED\n", varying_a, varying_b);
            return;
        default: {
            char error[ENO_MAX_ERROR_SIZE];
            sprintf(error, "Unimplemented error code %d.\n", instance->error.code);
            (*print_func)(error, varying_a, varying_b);
        }
    }
}

void print_to_memory(char *string, void *varying_a, void *varying_b)
{
    char *dest_buffer = varying_a;
    size_t *size = varying_b;

    strncpy(dest_buffer, string, *size);
}

void print_to_stream(char *string, void *varying_a, void *varying_b)
{
    FILE *stream = varying_a;
    (void)varying_b;

    fputs(string, stream);
}
