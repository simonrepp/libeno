#ifndef ENO_SERIALIZE_EXTERNAL_AST_H
#define ENO_SERIALIZE_EXTERNAL_AST_H

#include "eno.h"

void serialize_external_ast(ENOIterator *iterator, FILE *out_stream, unsigned int section_level);

#endif
