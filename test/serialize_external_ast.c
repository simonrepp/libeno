#include <stdio.h>

#include "serialize_external_ast.h"

void serialize_external_ast(ENOIterator *iterator, FILE *out_stream, unsigned int section_level)
{
    ENOIterator children_iterator;
    size_t size;
    char *token;

    while (eno_iterate_next(iterator)) {
        if (eno_is_attribute(iterator)) {
            eno_get_key(iterator, &token, &size);
            fprintf(out_stream, "%.*s=", (unsigned int)size, token);

            eno_get_value(iterator, &token, &size);
            if (token != NULL)
                fprintf(out_stream, "%.*s", (unsigned int)size, token);

            fputc('\n', out_stream);
        } else if (eno_is_embed(iterator)) {
            unsigned int dashes = 0;
            unsigned int offset;
            unsigned int max_dashes = 0;
            unsigned int two_dashes = 0;
            size_t value_size;
            char *value;

            eno_get_key(iterator, &token, &size);
            eno_get_value(iterator, &value, &value_size);

            if (value != NULL) {
                for (offset = 0; offset < value_size; offset++) {
                    if (value[offset] == '-') {
                        dashes++;
                    } else {
                        if (dashes > max_dashes)
                            max_dashes = dashes;
                        if (dashes == 2)
                            two_dashes = 1;
                        dashes = 0;
                    }
                }

                if (dashes > max_dashes)
                    max_dashes = dashes;

                if (dashes == 2 || two_dashes) {
                    dashes = max_dashes + 1;
                } else {
                    dashes = 2;
                }
            } else {
                dashes = 2;
            }

            for (offset = 0; offset < dashes; offset++)
                fputc('-', out_stream);
            fprintf(out_stream, "%.*s\n", (unsigned int)size, token);
            if (value != NULL)
                fprintf(out_stream, "%.*s\n", (unsigned int)value_size, value);
            for (offset = 0; offset < dashes; offset++)
                fputc('-', out_stream);
            fprintf(out_stream, "%.*s\n", (unsigned int)size, token);
        } else if (eno_is_field(iterator)) {
            eno_get_key(iterator, &token, &size);
            fprintf(out_stream, "%.*s:", (unsigned int)size, token);

            eno_get_value(iterator, &token, &size);
            if (token != NULL) {
                fprintf(out_stream, "%.*s\n", (unsigned int)size, token);
            } else {
                fputc('\n', out_stream);

                if (eno_iterate_children(iterator, &children_iterator))
                    serialize_external_ast(&children_iterator, out_stream, section_level);
            }
        } else if (eno_is_flag(iterator)) {
            eno_get_key(iterator, &token, &size);
            fprintf(out_stream, "%.*s\n", (unsigned int)size, token);
        } else if (eno_is_item(iterator)) {
            eno_get_value(iterator, &token, &size);
            fprintf(out_stream, "-%.*s\n", (unsigned int)size, token);
        } else if (eno_is_section(iterator)) {
            unsigned int hash;
            for (hash = 0; hash < section_level + 1; hash++) {
                fputc('#', out_stream);
            }

            eno_get_key(iterator, &token, &size);
            fprintf(out_stream, "%.*s\n", (unsigned int)size, token);

            eno_iterate_children(iterator, &children_iterator);
            serialize_external_ast(&children_iterator, out_stream, section_level + 1);
        }
    }
}
