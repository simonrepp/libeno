/*
 * Note: Everywhere an instruction (or an empty line as a special case) is printed
 *       the context->line_number is increased by 1, past that instruction's line
 */

#include "debug.h"
#include "eno.h"
#include "iterate.h"
#include "parse.h"

void debug_attribute(struct DebugContext *context, size_t offset)
{
    struct Attribute *attribute = (struct Attribute *)(context->instance->instructions + offset);
    
    debug_preceding_idle_instructions(context, attribute->line_number);

    if (context->format == FORMAT_TERMINAL) {
        if (attribute->id & HAS_VALUE) {
            struct AttributeWithValue *attribute_with_value = (struct AttributeWithValue *)attribute;

            printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s=%s%.*s%s%.*s%s%.*s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   attribute_with_value->line_number,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->key.begin - attribute_with_value->line.begin),
                   attribute_with_value->line.begin,
                   COLOR_KEY,
                   attribute_with_value->key.size,
                   attribute_with_value->key.begin,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->operator.begin - attribute_with_value->key.begin - attribute_with_value->key.size),
                   attribute_with_value->key.begin + attribute_with_value->key.size,
                   COLOR_OPERATOR,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->value.begin - attribute_with_value->operator.begin - attribute_with_value->operator.size),
                   attribute_with_value->operator.begin + attribute_with_value->operator.size,
                   COLOR_VALUE,
                   attribute_with_value->value.size,
                   attribute_with_value->value.begin,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->line.begin + attribute_with_value->line.size - attribute_with_value->value.begin - attribute_with_value->value.size),
                   attribute_with_value->value.begin + attribute_with_value->value.size);
        } else {
            printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s=%s%.*s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   attribute->line_number,
                   COLOR_RESET,
                   (unsigned int)(attribute->key.begin - attribute->line.begin),
                   attribute->line.begin,
                   COLOR_KEY,
                   attribute->key.size,
                   attribute->key.begin,
                   COLOR_RESET,
                   (unsigned int)(attribute->operator.begin - attribute->key.begin - attribute->key.size),
                   attribute->key.begin + attribute->key.size,
                   COLOR_OPERATOR,
                   COLOR_RESET,
                   (unsigned int)(attribute->line.begin + attribute->line.size - attribute->operator.begin - attribute->operator.size),
                   attribute->operator.begin + attribute->operator.size);
        }

    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }

    context->line_number = attribute->line_number + 1;
}

void debug_attribute_escaped(struct DebugContext *context, size_t offset)
{
    struct AttributeEscaped *attribute = (struct AttributeEscaped *)(context->instance->instructions + offset);
    
    debug_preceding_idle_instructions(context, attribute->line_number);

    if (context->format == FORMAT_TERMINAL) {
        if (attribute->id & HAS_VALUE) {
            struct AttributeEscapedWithValue *attribute_with_value = (struct AttributeEscapedWithValue *)attribute;

            printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s=%s%.*s%s%.*s%s%.*s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   attribute_with_value->line_number,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->escape_begin_operator.begin - attribute_with_value->line.begin),
                   attribute_with_value->line.begin,
                   COLOR_OPERATOR,
                   attribute_with_value->escape_begin_operator.size,
                   attribute_with_value->escape_begin_operator.begin,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->key.begin - attribute_with_value->escape_begin_operator.begin - attribute_with_value->escape_begin_operator.size),
                   attribute_with_value->escape_begin_operator.begin + attribute_with_value->escape_begin_operator.size,
                   COLOR_KEY,
                   attribute_with_value->key.size,
                   attribute_with_value->key.begin,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->escape_end_operator.begin - attribute_with_value->key.begin - attribute_with_value->key.size),
                   attribute_with_value->key.begin + attribute_with_value->key.size,
                   COLOR_OPERATOR,
                   attribute_with_value->escape_end_operator.size,
                   attribute_with_value->escape_end_operator.begin,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->operator.begin - attribute_with_value->escape_end_operator.begin - attribute_with_value->escape_end_operator.size),
                   attribute_with_value->escape_end_operator.begin + attribute_with_value->escape_end_operator.size,
                   COLOR_OPERATOR,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->value.begin - attribute_with_value->operator.begin - attribute_with_value->operator.size),
                   attribute_with_value->operator.begin + attribute_with_value->operator.size,
                   COLOR_VALUE,
                   attribute_with_value->value.size,
                   attribute_with_value->value.begin,
                   COLOR_RESET,
                   (unsigned int)(attribute_with_value->line.begin + attribute_with_value->line.size - attribute_with_value->value.begin - attribute_with_value->value.size),
                   attribute_with_value->value.begin + attribute_with_value->value.size);
           } else {
               printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s=%s%.*s\n",
                      COLOR_BRIGHT_BLACK_BACKGROUND,
                      context->line_number_padding,
                      attribute->line_number,
                      COLOR_RESET,
                      (unsigned int)(attribute->escape_begin_operator.begin - attribute->line.begin),
                      attribute->line.begin,
                      COLOR_OPERATOR,
                      attribute->escape_begin_operator.size,
                      attribute->escape_begin_operator.begin,
                      COLOR_RESET,
                      (unsigned int)(attribute->key.begin - attribute->escape_begin_operator.begin - attribute->escape_begin_operator.size),
                      attribute->escape_begin_operator.begin + attribute->escape_begin_operator.size,
                      COLOR_KEY,
                      attribute->key.size,
                      attribute->key.begin,
                      COLOR_RESET,
                      (unsigned int)(attribute->escape_end_operator.begin - attribute->key.begin - attribute->key.size),
                      attribute->key.begin + attribute->key.size,
                      COLOR_OPERATOR,
                      attribute->escape_end_operator.size,
                      attribute->escape_end_operator.begin,
                      COLOR_RESET,
                      (unsigned int)(attribute->operator.begin - attribute->escape_end_operator.begin - attribute->escape_end_operator.size),
                      attribute->escape_end_operator.begin + attribute->escape_end_operator.size,
                      COLOR_OPERATOR,
                      COLOR_RESET,
                      (unsigned int)(attribute->line.begin + attribute->line.size - attribute->operator.begin - attribute->operator.size),
                      attribute->operator.begin + attribute->operator.size);
           }
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }

    context->line_number = attribute->line_number + 1;
}

void debug_comment(struct DebugContext *context, size_t offset)
{
    struct Comment *comment = (struct Comment *)(context->instance->instructions + offset);
    
    if (context->format == FORMAT_TERMINAL) {
        if (comment->id & HAS_VALUE) {
            struct CommentWithValue *comment_with_value = (struct CommentWithValue *)(context->instance->instructions + offset);

            printf("%s   %*ld %s\t%s%.*s>%.*s%.*s%.*s%s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   comment_with_value->line_number,
                   COLOR_RESET,
                   COLOR_BRIGHT_BLACK,
                   (unsigned int)(comment_with_value->operator.begin - comment_with_value->line.begin),
                   comment_with_value->line.begin,
                   (unsigned int)(comment_with_value->value.begin - comment_with_value->operator.begin - 1),
                   comment_with_value->operator.begin + comment_with_value->operator.size,
                   comment_with_value->value.size,
                   comment_with_value->value.begin,
                   (unsigned int)(comment_with_value->line.begin + comment_with_value->line.size - comment_with_value->value.begin - comment_with_value->value.size),
                   comment_with_value->value.begin + comment_with_value->value.size,
                   COLOR_RESET);
        } else {
            printf("%s   %*ld %s\t%s%.*s>%.*s%s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   comment->line_number,
                   COLOR_RESET,
                   COLOR_BRIGHT_BLACK,
                   (unsigned int)(comment->operator.begin - comment->line.begin),
                   comment->line.begin,
                   (unsigned int)(comment->line.begin + comment->line.size - comment->operator.begin - comment->operator.size),
                   comment->operator.begin + comment->operator.size,
                   COLOR_RESET);
        }
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }
    
    context->line_number = comment->line_number + 1;
}

void debug_elements(struct DebugContext *context, size_t offset)
{
    while (offset != NONE) {
        struct Instruction *instruction = (struct Instruction *)(context->instance->instructions + offset);
        
        if (instruction->id & IS_ESCAPED) {
            if(instruction->id & IS_FLAG) {
                debug_flag_escaped(context, offset);
            } else /* if(instruction->id & IS_FIELD)  */ {
                debug_field_escaped(context, offset);
            }
        } else if(instruction->id & IS_EMBED) {
            debug_embed(context, offset);
        } else if(instruction->id & IS_FIELD) {
            debug_field(context, offset);
        } else if(instruction->id & IS_FLAG) {
            debug_flag(context, offset);
        } else if(instruction->id & IS_SECTION) {
            debug_section(context, offset);
        }

        offset = instruction->next_sibling_offset;
    }
}

void debug_embed(struct DebugContext *context, size_t offset)
{
    struct Embed *embed = (struct Embed *)(context->instance->instructions + offset);
    
    debug_preceding_idle_instructions(context, embed->line_number);

    if (context->format == FORMAT_TERMINAL) {
        printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s%.*s%s%.*s\n",
               COLOR_BRIGHT_BLACK_BACKGROUND,
               context->line_number_padding,
               embed->line_number,
               COLOR_RESET,
               (unsigned int)(embed->operator.begin - embed->line.begin),
               embed->line.begin,
               COLOR_OPERATOR,
               embed->operator.size,
               embed->operator.begin,
               COLOR_RESET,
               (unsigned int)(embed->key.begin - embed->operator.begin - embed->operator.size),
               embed->operator.begin + embed->operator.size,
               COLOR_KEY,
               embed->key.size,
               embed->key.begin,
               COLOR_RESET,
               (unsigned int)(embed->line.begin + embed->line.size - embed->key.begin - embed->key.size),
               embed->key.begin + embed->key.size);
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }

    debug_preceding_idle_instructions(context, embed->terminator_line_number);

    if (context->format == FORMAT_TERMINAL) {
        printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s%.*s%s%.*s\n",
               COLOR_BRIGHT_BLACK_BACKGROUND,
               context->line_number_padding,
               embed->terminator_line_number,
               COLOR_RESET,
               (unsigned int)(embed->terminator_operator.begin - embed->terminator_line.begin),
               embed->terminator_line.begin,
               COLOR_OPERATOR,
               embed->terminator_operator.size,
               embed->terminator_operator.begin,
               COLOR_RESET,
               (unsigned int)(embed->terminator_key.begin - embed->terminator_operator.begin - embed->terminator_operator.size),
               embed->terminator_operator.begin + embed->terminator_operator.size,
               COLOR_KEY,
               embed->terminator_key.size,
               embed->terminator_key.begin,
               COLOR_RESET,
               (unsigned int)(embed->terminator_line.begin + embed->terminator_line.size - embed->terminator_key.begin - embed->terminator_key.size),
               embed->terminator_key.begin + embed->terminator_key.size);
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        printf("   %*ld \t%.*s\n",
               context->line_number_padding,
               context->line_number,
               embed->terminator_line.size,
               embed->terminator_line.begin);
    }

    context->line_number = embed->terminator_line_number + 1;
}

void debug_empty_line(struct DebugContext *context)
{
    if (context->format == FORMAT_TERMINAL) {
        printf("%s   %*ld %s\t\n",
               COLOR_BRIGHT_BLACK_BACKGROUND,
               context->line_number_padding,
               context->line_number,
               COLOR_RESET);

    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        printf("   %*ld \t\n",
               context->line_number_padding,
               context->line_number);
    }

    context->line_number++;
}

void debug_field(struct DebugContext *context, size_t offset)
{
    struct Field *field = (struct Field *)(context->instance->instructions + offset);
    
    debug_preceding_idle_instructions(context, field->line_number);

    if (context->format == FORMAT_TERMINAL) {
        if (field->id & HAS_VALUE) {
            struct FieldWithValue *field_with_value = (struct FieldWithValue *)field;

            printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s:%s%.*s%s%.*s%s%.*s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   field_with_value->line_number,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->key.begin - field_with_value->line.begin),
                   field_with_value->line.begin,
                   COLOR_KEY,
                   field_with_value->key.size,
                   field_with_value->key.begin,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->operator.begin - field_with_value->key.begin - field_with_value->key.size),
                   field_with_value->key.begin + field_with_value->key.size,
                   COLOR_OPERATOR,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->value.begin - field_with_value->operator.begin - field_with_value->operator.size),
                   field_with_value->operator.begin + field_with_value->operator.size,
                   COLOR_KEY,
                   field_with_value->value.size,
                   field_with_value->value.begin,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->line.begin + field_with_value->line.size - field_with_value->value.begin - field_with_value->value.size),
                   field_with_value->value.begin + field_with_value->value.size);
        } else {
            printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s:%s%.*s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   field->line_number,
                   COLOR_RESET,
                   (unsigned int)(field->key.begin - field->line.begin),
                   field->line.begin,
                   COLOR_KEY,
                   field->key.size,
                   field->key.begin,
                   COLOR_RESET,
                   (unsigned int)(field->operator.begin - field->key.begin - field->key.size),
                   field->key.begin + field->key.size,
                   COLOR_OPERATOR,
                   COLOR_RESET,
                   (unsigned int)(field->line.begin + field->line.size - field->operator.begin - field->operator.size),
                   field->operator.begin + field->operator.size);
        }
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }

    context->line_number = field->line_number + 1;

    if (field->id & HAS_CHILDREN) {
        debug_field_children(context, ((struct FieldWithChildren *)field)->first_child_offset);
    }
}

void debug_field_children(struct DebugContext *context, size_t offset)
{
    while (offset != NONE) {
        struct Instruction *child = (struct Instruction *)(context->instance->instructions + offset);
        
        if (child->id & IS_ATTRIBUTE) {
            if (child->id & IS_ESCAPED) {
                debug_attribute_escaped(context, offset);
            } else {
                debug_attribute(context, offset);
            }
        } else /* if (child->id & IS_ITEM) */ {
            debug_item(context, offset);
        }
        
        offset = child->next_sibling_offset;
    }
}

void debug_field_escaped(struct DebugContext *context, size_t offset)
{
    struct FieldEscaped *field = (struct FieldEscaped *)(context->instance->instructions + offset);
    
    debug_preceding_idle_instructions(context, field->line_number);

    if (context->format == FORMAT_TERMINAL) {
        if (field->id & HAS_VALUE) {
            struct FieldEscapedWithValue *field_with_value = (struct FieldEscapedWithValue *)field;

            printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s:%s%.*s%s%.*s%s%.*s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   field_with_value->line_number,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->escape_begin_operator.begin - field_with_value->line.begin),
                   field_with_value->line.begin,
                   COLOR_OPERATOR,
                   field_with_value->escape_begin_operator.size,
                   field_with_value->escape_begin_operator.begin,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->key.begin - field_with_value->escape_begin_operator.begin - field_with_value->escape_begin_operator.size),
                   field_with_value->escape_begin_operator.begin + field_with_value->escape_begin_operator.size,
                   COLOR_KEY,
                   field_with_value->key.size,
                   field_with_value->key.begin,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->escape_end_operator.begin - field_with_value->key.begin - field_with_value->key.size),
                   field_with_value->key.begin + field_with_value->key.size,
                   COLOR_OPERATOR,
                   field_with_value->escape_end_operator.size,
                   field_with_value->escape_end_operator.begin,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->operator.begin - field_with_value->escape_end_operator.begin - field_with_value->escape_end_operator.size),
                   field_with_value->escape_end_operator.begin + field_with_value->escape_end_operator.size,
                   COLOR_OPERATOR,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->value.begin - field_with_value->operator.begin - field_with_value->operator.size),
                   field_with_value->operator.begin + field_with_value->operator.size,
                   COLOR_VALUE,
                   field_with_value->value.size,
                   field_with_value->value.begin,
                   COLOR_RESET,
                   (unsigned int)(field_with_value->line.begin + field_with_value->line.size - field_with_value->value.begin - field_with_value->value.size),
                   field_with_value->value.begin + field_with_value->value.size);
        } else {
            printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s:%s%.*s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   field->line_number,
                   COLOR_RESET,
                   (unsigned int)(field->escape_begin_operator.begin - field->line.begin),
                   field->line.begin,
                   COLOR_OPERATOR,
                   field->escape_begin_operator.size,
                   field->escape_begin_operator.begin,
                   COLOR_RESET,
                   (unsigned int)(field->key.begin - field->escape_begin_operator.begin - field->escape_begin_operator.size),
                   field->escape_begin_operator.begin + field->escape_begin_operator.size,
                   COLOR_KEY,
                   field->key.size,
                   field->key.begin,
                   COLOR_RESET,
                   (unsigned int)(field->escape_end_operator.begin - field->key.begin - field->key.size),
                   field->key.begin + field->key.size,
                   COLOR_OPERATOR,
                   field->escape_end_operator.size,
                   field->escape_end_operator.begin,
                   COLOR_RESET,
                   (unsigned int)(field->operator.begin - field->escape_end_operator.begin - field->escape_end_operator.size),
                   field->escape_end_operator.begin + field->escape_end_operator.size,
                   COLOR_OPERATOR,
                   COLOR_RESET,
                   (unsigned int)(field->line.begin + field->line.size - field->operator.begin - field->operator.size),
                   field->operator.begin + field->operator.size);
        }
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }

    context->line_number = field->line_number + 1;

    if (field->id & HAS_CHILDREN) {
        debug_field_children(context, ((struct FieldEscapedWithChildren *)field)->first_child_offset);
    }
}

void debug_flag(struct DebugContext *context, size_t offset)
{
    struct Flag *flag = (struct Flag *)(context->instance->instructions + offset);

    debug_preceding_idle_instructions(context, flag->line_number);

    if (context->format == FORMAT_TERMINAL) {
        printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s\n",
               COLOR_BRIGHT_BLACK_BACKGROUND,
               context->line_number_padding,
               flag->line_number,
               COLOR_RESET,
               (unsigned int)(flag->key.begin - flag->line.begin),
               flag->line.begin,
               COLOR_KEY,
               flag->key.size,
               flag->key.begin,
               COLOR_RESET,
               (unsigned int)(flag->line.begin + flag->line.size - flag->key.begin - flag->key.size),
               flag->key.begin + flag->key.size);
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }

    context->line_number = flag->line_number + 1;
}

void debug_flag_escaped(struct DebugContext *context, size_t offset)
{
    struct FlagEscaped *flag = (struct FlagEscaped *)(context->instance->instructions + offset);
    
    debug_preceding_idle_instructions(context, flag->line_number);

    if (context->format == FORMAT_TERMINAL) {
        printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s%s%.*s\n",
               COLOR_BRIGHT_BLACK_BACKGROUND,
               context->line_number_padding,
               flag->line_number,
               COLOR_RESET,
               (unsigned int)(flag->escape_begin_operator.begin - flag->line.begin),
               flag->line.begin,
               COLOR_OPERATOR,
               flag->escape_begin_operator.size,
               flag->escape_begin_operator.begin,
               COLOR_RESET,
               (unsigned int)(flag->key.begin - flag->escape_begin_operator.begin - flag->escape_begin_operator.size),
               flag->escape_begin_operator.begin + flag->escape_begin_operator.size,
               COLOR_KEY,
               flag->key.size,
               flag->key.begin,
               COLOR_RESET,
               (unsigned int)(flag->escape_end_operator.begin - flag->key.begin - flag->key.size),
               flag->key.begin + flag->key.size,
               COLOR_OPERATOR,
               flag->escape_end_operator.size,
               flag->escape_end_operator.begin,
               COLOR_RESET,
               (unsigned int)(flag->line.begin + flag->line.size - flag->escape_end_operator.begin - flag->escape_end_operator.size),
               flag->escape_end_operator.begin + flag->escape_end_operator.size);
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }

    context->line_number = flag->line_number + 1;
}

void debug_instruction(struct DebugContext *context, size_t offset)
{
    struct Instruction *instruction = (struct Instruction *)(context->instance->instructions + offset);
    
    printf("   %*ld \t%.*s\n",
           context->line_number_padding,
           context->line_number,
           instruction->line.size,
           instruction->line.begin);
}

void debug_item(struct DebugContext *context, size_t offset)
{
    struct Item *item = (struct Item *)(context->instance->instructions + offset);

    debug_preceding_idle_instructions(context, item->line_number);

    if (context->format == FORMAT_TERMINAL) {
        if (item->id & HAS_VALUE) {
            struct ItemWithValue *item_with_value = (struct ItemWithValue *)item;

            printf("%s   %*ld %s\t%.*s%s-%s%.*s%s%.*s%s%.*s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   item_with_value->line_number,
                   COLOR_RESET,
                   (unsigned int)(item_with_value->operator.begin - item_with_value->line.begin),
                   item_with_value->line.begin,
                   COLOR_OPERATOR,
                   COLOR_RESET,
                   (unsigned int)(item_with_value->value.begin - item_with_value->operator.begin - 1),
                   item_with_value->operator.begin + item_with_value->operator.size,
                   COLOR_VALUE,
                   item_with_value->value.size,
                   item_with_value->value.begin,
                   COLOR_RESET,
                   (unsigned int)(item_with_value->line.begin + item_with_value->line.size - item_with_value->value.begin - item_with_value->value.size),
                   item_with_value->value.begin + item_with_value->value.size);
        } else {
            printf("%s   %*ld %s\t%.*s%s-%s%.*s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   item->line_number,
                   COLOR_RESET,
                   (unsigned int)(item->operator.begin - item->line.begin),
                   item->line.begin,
                   COLOR_OPERATOR,
                   COLOR_RESET,
                   (unsigned int)(item->line.begin + item->line.size - item->operator.begin - item->operator.size),
                   item->operator.begin + item->operator.size);
        }
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }

    context->line_number = item->line_number + 1;
}

void debug_preceding_idle_instructions(struct DebugContext *context, unsigned long int before_line_number)
{
    while (context->line_number < before_line_number) {
        if (context->next_comment_offset != NONE) {
            struct Comment *comment = (struct Comment *)(context->instance->instructions + context->next_comment_offset);
            
            if (comment->line_number == context->line_number) {
                debug_comment(context, context->next_comment_offset);
                context->next_comment_offset = comment->next_sibling_offset;
                continue;
            }
        }
        
        debug_empty_line(context);
    }
}

void debug_section(struct DebugContext *context, size_t offset)
{
    struct Section *section = (struct Section *)(context->instance->instructions + offset);

    debug_preceding_idle_instructions(context, section->line_number);

    if (context->format == FORMAT_TERMINAL) {
        printf("%s   %*ld %s\t%.*s%s%.*s%s%.*s%s%.*s%s%.*s\n",
               COLOR_BRIGHT_BLACK_BACKGROUND,
               context->line_number_padding,
               section->line_number,
               COLOR_RESET,
               (unsigned int)(section->operator.begin - section->line.begin),
               section->line.begin,
               COLOR_OPERATOR,
               section->operator.size,
               section->operator.begin,
               COLOR_RESET,
               (unsigned int)(section->key.begin - section->operator.begin - section->operator.size),
               section->operator.begin + section->operator.size,
               COLOR_KEY,
               section->key.size,
               section->key.begin,
               COLOR_RESET,
               (unsigned int)(section->line.begin + section->line.size - section->key.begin - section->key.size),
               section->key.begin + section->key.size);
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        debug_instruction(context, offset);
    }

    context->line_number = section->line_number + 1;

    debug_elements(context, section->first_child_offset);
}

void debug_unparsed(struct DebugContext *context, size_t offset)
{
    while (offset != NONE) {
        struct Instruction *instruction = (struct Instruction *)(context->instance->instructions + offset);
        
        debug_preceding_idle_instructions(context, instruction->line_number);

        if (context->format == FORMAT_TERMINAL) {
            printf("%s   %*ld %s\t%s%.*s%s\n",
                   COLOR_BRIGHT_BLACK_BACKGROUND,
                   context->line_number_padding,
                   instruction->line_number,
                   COLOR_RESET,
                   COLOR_BRIGHT_BLACK,
                   instruction->line.size,
                   instruction->line.begin,
                   COLOR_RESET);
        } else /* if(context->format == FORMAT_PLAINTEXT) */ {
            debug_instruction(context, offset);
        }

        context->line_number = instruction->line_number + 1;

        offset = instruction->next_sibling_offset;
    }
}

/**
 * Utilizes the AST obtained from the document to print back the document for debugging purposes
 *
 * The document is printed as it was read, however annotated with line numbers.
 */
void eno_debug_document(ENOIterator *opaque_iterator)
{
    struct DebugContext context;
    struct Instance *instance = ((struct Iterator *)opaque_iterator)->instance;
    struct Document *document = (struct Document *)instance->instructions;

    context.format = FORMAT_TERMINAL;
    context.instance = instance;
    context.line_number = 1;
    context.line_number_padding = line_number_padding(instance->input_linecount);
    context.next_comment_offset = document->first_comment_offset;

    /* TODO: Encapsulate printing the header */
    if (context.format == FORMAT_TERMINAL) {
        printf("%s   %.*s %s\t%sFile%s\n",
               COLOR_BLACK COLOR_WHITE_BACKGROUND,
               context.line_number_padding,
               "          ",
               COLOR_RESET,
               COLOR_BOLD,
               COLOR_RESET);
    } else /* if(context->format == FORMAT_PLAINTEXT) */ {
        printf("   %.*s \tFile\n",
               context.line_number_padding,
               "          ");
    }

    debug_elements(&context, 0); /* offset 0 always points to the document */
    debug_unparsed(&context, document->first_unparsed_instruction_offset);
    debug_preceding_idle_instructions(&context, instance->input_linecount - 1);
}

unsigned int line_number_padding(unsigned long int num_lines)
{
    if (num_lines < 1000) return 3;
    if (num_lines < 10000) return 4;
    if (num_lines < 100000) return 5;
    if (num_lines < 1000000) return 6;
    if (num_lines < 10000000) return 6;
    if (num_lines < 100000000) return 8;
    if (num_lines < 1000000000) return 9;

    return 10;
}
