#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "eno.h"
#include "helpers.h"
#include "serialize_external_ast.h"
#include "serialize_internal_ast.h"

#define EXAMPLES_DIR "../test/examples"

int verify_external_ast(char *example_path, char *spec_path, unsigned int update_specs);
int verify_internal_ast(char *example_path, char *spec_path, unsigned int update_specs);

int main()
{
    char example_path[512];
    DIR *examples_dir;
    struct dirent *examples_dirent;
    DIR *spec_dir;
    char spec_path[1024];
    struct stat statbuf;
    unsigned int update_specs = getenv("UPDATE_SPECS") != NULL;

    examples_dir = opendir(EXAMPLES_DIR);

    while ((examples_dirent = readdir(examples_dir)) != NULL) {
        if (examples_dirent->d_name[0] == '.')
            continue;

        sprintf(example_path, "%s/%s", EXAMPLES_DIR, examples_dirent->d_name);

        if (stat(example_path, &statbuf) || S_ISDIR(statbuf.st_mode))
            continue;

        fprintf(stderr, "%s\n", example_path);

        sprintf(spec_path,
                "%s/%.*s.spec",
                EXAMPLES_DIR,
                (unsigned int)(strlen(examples_dirent->d_name) - strlen(".eno")),
                examples_dirent->d_name);

        if (!(spec_dir = opendir(spec_path))) {
            mkdir(spec_path, 0755);
        } else {
            closedir(spec_dir);
        }

        if (!verify_internal_ast(example_path, spec_path, update_specs))
            return EXIT_FAILURE;

        if (!verify_external_ast(example_path, spec_path, update_specs))
            return EXIT_FAILURE;
    }

    closedir(examples_dir);

    fprintf(stderr, "%sAll examples passed.%s\n", COLOR_GREEN, COLOR_RESET);

    return EXIT_SUCCESS;
}

/**
 * TODO: Outdated description.
 *
 * Re-serializes the input document using only enolib's public API
 * for reading the document's AST. In the resulting serialized
 * output all unnecessary whitespace is omitted, all copied elements
 * and hierarchies are flattened and replaced with hard copies, and
 * all continuations are combined into single value tokens. Escaping
 * may be modified or dropped, thus differing from the input document.
 * The output is then written to disk under the original filename,
 * but with a modified extension (.parsed.eno). If that file already
 * exists its previously written content is instead compared to the
 * current result and when it does not match, verification fails.
 */
int verify_external_ast(char *example_path, char *spec_path, unsigned int update_specs)
{
    FILE *diff_file;
    char diff_path[512];
    FILE *example_file;
    ENOIterator example_iterator;
    FILE *out_file;
    char out_path[512];
    unsigned int success;

    example_file = fopen(example_path, "rb");

    if ((success = eno_parse_stream(&example_iterator, example_file))) {
        sprintf(out_path, "%s/external_ast.eno", spec_path);

        if (update_specs || !(out_file = fopen(out_path, "rb"))) {
            out_file = fopen(out_path, "wb");
            serialize_external_ast(&example_iterator, out_file, 0);
        } else {
            sprintf(diff_path, "%s/external_ast.diff.eno", spec_path);

            diff_file = fopen(diff_path, "wb");
            serialize_external_ast(&example_iterator, diff_file, 0);
            fclose(diff_file);

            diff_file = fopen(diff_path, "rb");
            if ((success = diff(out_file, diff_file))) {
                fclose(diff_file);
                remove(diff_path);
            } else {
                fclose(diff_file);
                fprintf(stderr,
                        "Expected: %s%s%s\nReceived: %s%s%s\n\n%sAn example failed during external AST verification.%s\n",
                        COLOR_RED,
                        out_path,
                        COLOR_RESET,
                        COLOR_GREEN,
                        diff_path,
                        COLOR_RESET,
                        COLOR_RED,
                        COLOR_RESET);
            }
        }

        fclose(out_file);
    } else {
        fprintf(stderr, "FAILURE: %s contains an error.\n", example_path);
    }

    eno_free_document(&example_iterator);
    fclose(example_file);

    return success;
}

/**
 * Serializes the internal AST constructed during parsing to a
 * meta-format that exactly matches the original document, except
 * that an interpunct (·) is added in between all lexical tokens,
 * including ignored whitespace ranges, and a line-feed symbol
 * character (␊) added at the end of every line. The serialized
 * output is written to example.spec/internal_ast.txt if it does
 * not yet exist. If it does, output is instead written to
 * example.spec/internal_ast.diff.txt and the content of both
 * files compared. If they don't match, verification has failed,
 * and the files can be inspected to identify the issue in the
 * library code. If they match, the diff file is removed, as it
 * is identical to the previously written spec anyway.
 */
int verify_internal_ast(char *example_path, char *spec_path, unsigned int update_specs)
{
    FILE *diff_file;
    char diff_path[512];
    ENOIterator example_iterator;
    FILE *example_file;
    FILE *out_file;
    char out_path[512];
    unsigned int success;

    example_file = fopen(example_path, "rb");

    if ((success = eno_parse_stream(&example_iterator, example_file))) {
        sprintf(out_path, "%s/internal_ast.txt", spec_path);

        if (update_specs || !(out_file = fopen(out_path, "rb"))) {
            out_file = fopen(out_path, "wb");
            serialize_internal_ast_document(&example_iterator, out_file);
        } else {
            sprintf(diff_path, "%s/internal_ast.diff.txt", spec_path);

            diff_file = fopen(diff_path, "wb");
            serialize_internal_ast_document(&example_iterator, diff_file);
            fclose(diff_file);

            diff_file = fopen(diff_path, "rb");
            if ((success = diff(out_file, diff_file))) {
                fclose(diff_file);
                remove(diff_path);
            } else {
                fclose(diff_file);
                fprintf(stderr,
                        "Expected: %s%s%s\nReceived: %s%s%s\n\n%sAn example failed during internal AST verification.%s\n",
                        COLOR_RED,
                        out_path,
                        COLOR_RESET,
                        COLOR_GREEN,
                        diff_path,
                        COLOR_RESET,
                        COLOR_RED,
                        COLOR_RESET);
            }
        }

        fclose(out_file);
    } else {
        fprintf(stderr, "FAILURE: %s contains an error.\n", example_path);
    }

    eno_free_document(&example_iterator);
    fclose(example_file);

    return success;
}
