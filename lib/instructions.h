/*
 * The char * pointers inside the Token structs are used to
 * point into a single contiguous unicode text buffer that
 * holds all user tokens found in the document.
 *
 * All instructions share a varying amount of a leading memory layout,
 * this allows to begin allocating an instruction's more generic fields
 * during parsing and reinterpret the memory as a more specific type
 * later, adding additional, specific fields after the generic ones.
 */

#ifndef ENO_INSTRUCTIONS_H
#define ENO_INSTRUCTIONS_H

#include <stddef.h>

/*
 * 16bit id bitmask used on all instructions:
 * .xxx xxxx xxxx xxxx
 */

#define HAS_ATTRIBUTES 0x1
#define HAS_CHILDREN 0x2
#define HAS_ITEMS 0x4
#define HAS_KEY 0x8

#define HAS_VALUE 0x10
#define IS_ATTRIBUTE 0x20
#define IS_COMMENT 0x40
#define IS_DOCUMENT 0x80

#define IS_EMBED 0x100
#define IS_ESCAPED 0x200
#define IS_FIELD 0x400
#define IS_FLAG 0x800

#define IS_ITEM 0x1000
#define IS_SECTION 0x2000
#define IS_UNPARSED 0x4000

/**
 * Pointers to instructions (which are all inside one contiguous instructions buffer memory region)
 * are stored as memory offsets of type size_t. This is done because the memory region can be
 * reallocated to grow the buffer, which would invalidate addresses, but not offsets.
 * As we obviously can't use NULL (0) as a special marker for offsets though, we introduce NONE
 * as our NULL-equivalent special marker, which is the maximum value of size_t (and which for a
 * number of reasons will never collide with an actual offset we are storing).
 */
#define NONE (size_t)-1

/**
 * In order to indicate a token as missing, set begin to NULL,
 * size can be left uninitialized in that case.
 */
struct Token {
    char *begin;
    unsigned int size;
};

/* TODO: Given unparsed instructions now have a type we could chain them into the regular AST tree as well */
/*
 * All instruction structs start with this memory-compatible field layout.
 */
struct Instruction {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;
};

/*
 * All instructions that have an escaped key need to match this memory layout:
 * - AttributeEscaped
 * - AttributeEscapedWithValue
 * - FieldEscaped
 * - FieldEscapedWithChildren
 * - FieldEscapedWithValue
 * - FlagEscaped
 */
struct AttributeFieldFlagEscaped {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token escape_begin_operator;
    struct Token escape_end_operator;
};

/*
 * All instructions that have a key need to match this memory layout:
 * - Attribute
 * - AttributeEscaped
 * - AttributeEscapedWithValue
 * - AttributeWithValue
 * - Embed
 * - Field
 * - FieldEscaped
 * - FieldEscapedWithChildren
 * - FieldEscapedWithValue
 * - FieldWithChildren
 * - FieldWithValue
 * - Flag
 * - FlagEscaped
 * - Section
 */
struct InstructionWithKey {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;
};

/* key = */
struct Attribute {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token operator;
};

/* `key` = */
struct AttributeEscaped {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token escape_begin_operator;
    struct Token escape_end_operator;

    struct Token operator;
};

/* `key` = value */
struct AttributeEscapedWithValue {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token escape_begin_operator;
    struct Token escape_end_operator;

    struct Token operator;

    struct Token value;
};

/* key = value */
struct AttributeWithValue {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token operator;

    struct Token value;
};

/* > */
struct Comment {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset; /* next comment */

    struct Token operator;
};

/* > value */
struct CommentWithValue {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset; /* next comment */

    struct Token operator;
    struct Token value;
};

struct Document {
    unsigned short id;
    unsigned long int _unused_line_number;
    struct Token _unused_line;
    size_t next_sibling_offset;

    size_t first_comment_offset;
    size_t first_unparsed_instruction_offset;
};

/* -- key */
struct Embed {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token operator;
    struct Token value;
    unsigned long int terminator_line_number;
    struct Token terminator_line;
    struct Token terminator_key;
    struct Token terminator_operator;
};

/* key: */
struct Field {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token operator;
};

/* `key`: */
struct FieldEscaped {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token escape_begin_operator;
    struct Token escape_end_operator;

    struct Token operator;
};

/* `key`: */
struct FieldEscapedWithChildren {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token escape_begin_operator;
    struct Token escape_end_operator;

    struct Token operator;

    size_t first_child_offset;
};

/* `key`: value */
struct FieldEscapedWithValue {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token escape_begin_operator;
    struct Token escape_end_operator;

    struct Token operator;

    struct Token value;
};

/* key: */
struct FieldWithChildren {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token operator;

    size_t first_child_offset;
};

/* key: value */
struct FieldWithValue {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token operator;

    struct Token value;
};

/* key */
struct Flag {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;
};

/* `key` */
struct FlagEscaped {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token escape_begin_operator;
    struct Token escape_end_operator;
};

/* - */
struct Item {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token operator;
};

/* - value */
struct ItemWithValue {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token operator;

    struct Token value;
};


/* # key */
struct Section {
    unsigned short id;
    unsigned long int line_number;
    struct Token line;
    size_t next_sibling_offset;

    struct Token key;

    struct Token operator;
    size_t first_child_offset;
};

/**
 * It's ABSOLUTELY CRITICAL that this union be kept up-to-date with all
 * instruction structs. It's sole purpose is to execute sizeof() on it in order
 * to determine the largest possible instruction we possibly need to allocate.
 * (which is used for conditionally growing the instructions buffer during
 * parsing the document).
 */
union AllInstructions {
    struct Instruction _Instruction;
    struct Attribute _Attribute;
    struct AttributeEscaped _AttributeEscaped;
    struct AttributeEscapedWithValue _AttributeEscapedWithValue;
    struct AttributeWithValue _AttributeWithValue;
    struct Comment _Comment;
    struct CommentWithValue _CommentWithValue;
    struct Embed _Embed;
    struct Field _Field;
    struct FieldEscaped _FieldEscaped;
    struct FieldEscapedWithValue _FieldEscapedWithValue;
    struct FieldWithValue _FieldWithValue;
    struct Flag _Flag;
    struct FlagEscaped _FlagEscaped;
    struct Item _Item;
    struct ItemWithValue _ItemWithValue;
    struct Section _Section;
};

#endif
