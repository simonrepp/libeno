#ifndef ENO_DEBUG_H
#define ENO_DEBUG_H

#include "instance.h"
#include "instructions.h"

#define COLOR_RESET "\x1b[0m"
#define COLOR_BOLD "\x1b[1m"
#define COLOR_DIM "\x1b[2m"

#define COLOR_BLACK "\x1b[30m"
#define COLOR_BRIGHT_BLACK "\x1b[90m"
#define COLOR_WHITE "\x1b[37m"
#define COLOR_BRIGHT_WHITE "\x1b[97m"

#define COLOR_BRIGHT_BLACK_BACKGROUND "\x1b[40m"
#define COLOR_BRIGHT_RED_BACKGROUND "\x1b[101m"
#define COLOR_WHITE_BACKGROUND "\x1b[47m"

#define COLOR_KEY COLOR_BOLD COLOR_BRIGHT_WHITE
#define COLOR_OPERATOR COLOR_WHITE
#define COLOR_VALUE COLOR_DIM COLOR_WHITE

enum Format {
    FORMAT_PLAINTEXT,
    FORMAT_TERMINAL
};

struct DebugContext {
    enum Format format;
    struct Instance *instance;
    unsigned long int line_number;
    unsigned int line_number_padding;
    size_t next_comment_offset;
};

void debug_attribute(struct DebugContext *context, size_t offset);
void debug_attribute_escaped(struct DebugContext *context, size_t offset);
void debug_comment(struct DebugContext *context, size_t offset);
void debug_elements(struct DebugContext *context, size_t offset);
void debug_embed(struct DebugContext *context, size_t offset);
void debug_empty_line(struct DebugContext *context);
void debug_field(struct DebugContext *context, size_t offset);
void debug_field_children(struct DebugContext *context, size_t offset);
void debug_field_escaped(struct DebugContext *context, size_t offset);
void debug_flag(struct DebugContext *context, size_t offset);
void debug_flag_escaped(struct DebugContext *context, size_t offset);
void debug_instruction(struct DebugContext *context, size_t offset);
void debug_item(struct DebugContext *context, size_t offset);
void debug_preceding_idle_instructions(struct DebugContext *context, unsigned long int before_line_number);
void debug_section(struct DebugContext *context, size_t offset);
void debug_unparsed(struct DebugContext *context, size_t offset);
unsigned int line_number_padding(unsigned long int num_lines);

#endif
